#include "libitm.h"
#include "thread.h"
#include "config/common.h"
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
#include "api/api.hpp"
#include "stm/txthread.hpp"
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
#include "stm.h"
#include "wrappers.h"
#else
#error "unknown or no stm for phasedtm specified!"
#endif


/*
		threadDescriptor_t* tx = getThreadDescriptor(); \
		void *top = mask_stack_top(tx); \
		void *bot = mask_stack_bottom(); \
		void *ptr = (void*)addr; \
		if (ptr < top && ptr >= bot) return *addr; \
		
		threadDescriptor_t* tx = getThreadDescriptor(); \
		void *top = mask_stack_top(tx); \
		void *bot = mask_stack_bottom(); \
		void *ptr = (void*)addr; \
		if (ptr < top && ptr >= bot) { \
			*addr = value; \
		} else { \
			stm::stm_write(addr, value, (stm::TxThread*)stm::Self); \
		}	\
	*/

#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
#define ITM_READ(TYPE, LSMOD, EXT) \
	ITM_REGPARM TYPE _ITM_##LSMOD##EXT (const TYPE *addr) { \
		return stm::stm_read(addr, (stm::TxThread*)stm::Self); \
	}

#define ITM_WRITE(TYPE, LSMOD, EXT) \
	ITM_REGPARM void _ITM_##LSMOD##EXT (TYPE *addr, TYPE value) { \
		stm::stm_write(addr, value, (stm::TxThread*)stm::Self); \
	}
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
#define ITM_READ(TYPE, LSMOD, FUNC, EXT) \
 ITM_REGPARM TYPE _ITM_##LSMOD##EXT (const TYPE *addr) { \
   return (TYPE)FUNC((volatile TYPE*)addr); \
 }

#define ITM_WRITE(TYPE, LSMOD, FUNC, EXT) \
 ITM_REGPARM void _ITM_##LSMOD##EXT (TYPE *addr, TYPE value) { \
   FUNC((volatile TYPE*)addr, value); \
 }
#else
#error "unknown or no stm for phasedtm specified!"
#endif


static void memtransfer (void* dst, const void* src, size_t size) {
  unsigned char* src_ptr = (unsigned char*)src;
  unsigned char* dst_ptr = (unsigned char*)dst;
  for (size_t i=0; i < size; i++) {
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
    unsigned char c = stm::stm_read(src_ptr, (stm::TxThread*)stm::Self);
    stm::stm_write(dst_ptr, c, (stm::TxThread*)stm::Self);
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
    unsigned char c = stm_load_uchar((volatile unsigned char*)src_ptr);
    stm_store_uchar((volatile unsigned char*)dst_ptr, c);
#else
#error "unknown or no stm for phasedtm specified!"
#endif
    src_ptr++;
    dst_ptr++;
  }
}

#define ITM_VECTOR_READ(TYPE, LSMOD, EXT) \
  ITM_REGPARM TYPE _ITM_##LSMOD##EXT (const TYPE *addr) { \
    TYPE v; \
    memtransfer(&v, addr, sizeof(TYPE)); \
    return v; \
  }

#define ITM_VECTOR_WRITE(TYPE, LSMOD, EXT) \
  ITM_REGPARM void _ITM_##LSMOD##EXT (TYPE *addr, TYPE value) { \
    memtransfer(addr, &value, sizeof(TYPE)); \
  }

// ABI Section 5.12
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
#define ITM_BARRIERS(TYPE, EXT) \
  ITM_READ(TYPE, R, EXT) \
  ITM_READ(TYPE, RaR, EXT) \
  ITM_READ(TYPE, RaW, EXT) \
  ITM_READ(TYPE, RfW, EXT) \
  ITM_WRITE(TYPE, W, EXT) \
  ITM_WRITE(TYPE, WaR, EXT) \
  ITM_WRITE(TYPE, WaW, EXT)
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
#define ITM_BARRIERS(FEXT, TYPE, EXT) \
  ITM_READ(TYPE, R, stm_load_##FEXT, EXT) \
  ITM_READ(TYPE, RaR, stm_load_##FEXT, EXT) \
  ITM_READ(TYPE, RaW, stm_load_##FEXT, EXT) \
  ITM_READ(TYPE, RfW, stm_load_##FEXT, EXT) \
  ITM_WRITE(TYPE, W, stm_store_##FEXT, EXT) \
  ITM_WRITE(TYPE, WaR, stm_store_##FEXT, EXT) \
  ITM_WRITE(TYPE, WaW, stm_store_##FEXT, EXT)
#else
#error "unknown or no stm for phasedtm specified!"
#endif

#define ITM_VECTOR_BARRIERS(TYPE, EXT) \
  ITM_VECTOR_READ(TYPE, R, EXT) \
  ITM_VECTOR_READ(TYPE, RaR, EXT) \
  ITM_VECTOR_READ(TYPE, RaW, EXT) \
  ITM_VECTOR_READ(TYPE, RfW, EXT) \
  ITM_VECTOR_WRITE(TYPE, W, EXT) \
  ITM_VECTOR_WRITE(TYPE, WaR, EXT) \
  ITM_VECTOR_WRITE(TYPE, WaW, EXT)

#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
ITM_BARRIERS (uint8_t, U1)
ITM_BARRIERS (uint16_t, U2)
ITM_BARRIERS (uint32_t, U4)
ITM_BARRIERS (uint64_t, U8)
ITM_BARRIERS (float, F)
ITM_BARRIERS (double, D)
//ITM_BARRIERS (long double, E)
//ITM_BARRIERS (float _Complex, CF)
//ITM_BARRIERS (double _Complex, CD)
//ITM_BARRIERS (long double _Complex, CE)
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
ITM_BARRIERS (u8, uint8_t, U1)
ITM_BARRIERS (u16, uint16_t, U2)
ITM_BARRIERS (u32, uint32_t, U4)
ITM_BARRIERS (u64, uint64_t, U8)
ITM_BARRIERS (float, float, F)
ITM_BARRIERS (double, double, D)
#else
#error "unknown or no stm for phasedtm specified!"
#endif

#if defined(__i386__) || defined(__x86_64__)
# ifdef __MMX__
	ITM_VECTOR_BARRIERS (__m64, M64)
# endif
# ifdef __SSE__
	ITM_VECTOR_BARRIERS (__m128, M128)
# endif
# ifdef __AVX__
	ITM_VECTOR_BARRIERS (__m256, M256)
# endif
#endif													/* i386 */

#undef ITM_BARRIERS
