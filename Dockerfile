FROM ubuntu:20.04

ARG dependencies="build-essential binutils-dev cmake libboost-all-dev llvm libnuma-dev gdb vim"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y $dependencies

WORKDIR /app
