import re
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def get_TM_time_data(filename):
	# open and read file
	log_data = open(filename).read()
	# get all times from the same type: app,input,thread,
	timers = []
	timer_regex = re.compile(r'Elapsed time: (\d*.\d*)')
	timers = [float(i) for i in re.findall(timer_regex, log_data)]
	
	return timers

def get_HW_TM_data(filename):
	# open and read file
	log_data = open(filename).read()
	

	# get hardware usage percentage
	hws = []
	hw_regex = re.compile(r'hw:\s*(\d*.\d*)')
	hws = [float(i) for i in re.findall(hw_regex, log_data)]
	return hws


#Provide inputs address
addr_TM = "../GoTM/gotm-tm-experiments/results/"

# run for all apps 
_APPS_TM = ["sssp","mst"]
#run for all grapfs
_INPUTS = ["rmat-n30m-m120m","soc-LiveJournal1"]
#run for all threads
_THREADS = ["2","4","8","12","18"]
#run for all runtimes
_RUNTIMES = ["GoTM", "CTM", "PhTMs"]

time_GoTM = {}
time_phtm = {}
time_CTM = {}
time_value_GoTM = []
time_value_phtm = []
time_value_CTM = []
time_std_GoTM = []
time_std_phtm = []
time_std_CTM = []

degree_xlabel = _INPUTS
apps_ylabel = _APPS_TM
f, (p) = plt.subplots(len(_APPS_TM), len(_INPUTS), sharex='col', figsize=[8,8])
subplot_inx=0
nruns = 5
for app_index in range(0,len(_APPS_TM)):
	offset = 0.1
	
	results_GoTM = []
	results_CTM = []
	results_phTM = []
	time_references = []
	ref_index = 0
	res_index = 0
	#for sequential
	for inputs in _INPUTS:
		#for time reference (baseline) outputTM-sssp_TM-seq-soc-LiveJournal1
		filename = "../GoTM/gotm-tm-experiments/results/"+"outputTM-"+_APPS_TM[app_index]+"_TM-seq-"+inputs
		temp = get_TM_time_data(filename)
		time_references.append(sum(temp)/len(temp))
		
	#for GoTM outputTM-sssp_TM-gotm-soc-LiveJournal1-18
	addr_GoTM = addr_TM
	for inputs in _INPUTS:
		for threads in _THREADS:
			filename = addr_GoTM+"/outputTM-"+_APPS_TM[app_index]+"_TM-gotm-"+inputs+"-"+threads
			temp = get_TM_time_data(filename)
			#if threads == "1":
			#	time_references.append(sum(temp)/len(temp))
			results_GoTM.append(temp[0:nruns])
	
	#for PhTM
	addr_phTM = addr_TM
	for inputs in _INPUTS:
		#temp = (1,1,1,1,1,1,1,1,1,1)
		#results_phTM.append(temp[0:nruns])
		for threads in _THREADS:
			filename = addr_phTM+"/outputTM-"+_APPS_TM[app_index]+"_TM-phtms-"+inputs+"-"+threads
			temp = get_TM_time_data(filename)
			results_phTM.append(temp[0:nruns])
	
	#for CTM
	addr_CTM = addr_TM+_RUNTIMES[1]
	for inputs in _INPUTS:
		#temp = (1,1,1,1,1,1,1,1,1,1)
		#results_CTM.append(temp[0:nruns])
		for threads in _THREADS:
			filename = addr_CTM+"/outputTM-"+_APPS_TM[app_index]+"_TM-cttm-"+inputs+"-"+threads
			results = get_TM_time_data(filename)
			results_CTM.append(results[0:nruns])
	
	for i in range(len(_INPUTS)):		
		normalized_speedup_GoTM = []
		normalized_speedup_phTM = []
		normalized_speedup_CTM = []
		
		
		for j in range(len(_THREADS)):		
			#if results_GoTM[res_index]:
			temp = []
			for x in results_GoTM[res_index]:
				temp.append(time_references[ref_index]/x)
			normalized_speedup_GoTM.append(temp)
			
			#else:
			#	normalized_speedup_GoTM.append(0)
				
			#if results_phTM[res_index][0]:
			temp = []
			for x in results_phTM[res_index]:
				temp.append(time_references[ref_index]/x)
			normalized_speedup_phTM.append(temp)
			#else:
			#	normalized_speedup_phTM.append(0)

			#if results_CTM[res_index][0]:
			temp = []
			for x in results_CTM[res_index]:
				temp.append(time_references[ref_index]/x)
			normalized_speedup_CTM.append(temp)
			#else:
			#	normalized_speedup_CTM.append(0)
			
			res_index = res_index + 1
		ref_index = ref_index + 1
		
		# GoTM plots
		time_GoTM[subplot_inx] = pd.DataFrame(np.c_[normalized_speedup_GoTM[0], normalized_speedup_GoTM[1], normalized_speedup_GoTM[2], normalized_speedup_GoTM[3], normalized_speedup_GoTM[4]], columns=_THREADS)
		time_value_GoTM.append(time_GoTM[subplot_inx].mean())
		time_std_GoTM.append(time_GoTM[subplot_inx].std())
		
		#phTM plots
		time_phtm[subplot_inx] = pd.DataFrame(np.c_[normalized_speedup_phTM[0], normalized_speedup_phTM[1], normalized_speedup_phTM[2], normalized_speedup_phTM[3], normalized_speedup_phTM[4]], columns=_THREADS)
		time_value_phtm.append(time_phtm[subplot_inx].mean())
		time_std_phtm.append(time_phtm[subplot_inx].std())
		
		# cttm plots 
		time_CTM[subplot_inx] = pd.DataFrame(np.c_[normalized_speedup_CTM[0], normalized_speedup_CTM[1], normalized_speedup_CTM[2], normalized_speedup_CTM[3], normalized_speedup_CTM[4]], columns=_THREADS)
		time_value_CTM.append(time_CTM[subplot_inx].mean())
		time_std_CTM.append(time_CTM[subplot_inx].std())
		
		
		p1 = p[app_index][i].bar(np.arange(-3*offset, len(time_CTM[subplot_inx].columns)-3*offset), time_value_CTM[subplot_inx],error_kw=dict(lw=0.5, capsize=0.5, capthick=0.3),width=offset,color="tomato",label="CTM",edgecolor='k')
		p2 = p[app_index][i].bar(np.arange(-2*offset, len(time_GoTM[subplot_inx].columns)-2*offset), time_value_GoTM[subplot_inx],error_kw=dict(lw=0.5, capsize=0.5, capthick=0.3), width=offset, color="blue",label="GoTM",edgecolor='k')
		p3 = p[app_index][i].bar(np.arange(-offset, len(time_phtm[subplot_inx].columns)-offset), time_value_phtm[subplot_inx],error_kw=dict(lw=0.5, capsize=0.5, capthick=0.3),width=offset, color="green", label="PhTM*",edgecolor='k')
		
		
		p[app_index][i].set_xticks(range(len(time_GoTM[subplot_inx].columns)))
		p[app_index][i].set_xticklabels(time_GoTM[subplot_inx].columns, fontsize=6)
		p[app_index][i].xaxis.label.set_visible(True)
		p[app_index][i].tick_params(axis="y", labelsize=6)
		p[app_index][i].axhline(y=1,linewidth=0.3, color='k', linestyle='--')
		
		
		subplot_inx = subplot_inx + 1


pad = 1
for ax, d in zip(p[0], degree_xlabel):
    ax.annotate(d, xy=(0.5, 1), xytext=(0, pad),xycoords='axes fraction', textcoords='offset points',size='large', ha='center', va='baseline', fontsize=8, fontname='serif')

for ax, a in zip(p[:,0], apps_ylabel):
    ax.annotate(a, xy=(0, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),xycoords=ax.yaxis.label, textcoords='offset points',size='large', ha='right', va='center', fontsize=8, rotation=90, fontname='serif', fontweight='bold')
    ax.set_ylabel('Normalized\nspeedup', rotation=90, fontsize=6,fontname='serif')

for ax in p[app_index]:   
	ax.set_xlabel('Threads', fontsize=6,fontname='serif')	

f.legend((p1[0],p2[0],p3[0]),("CTM",'GoTM','PhTM*'),frameon=False,loc="upper left",labelspacing=0, borderaxespad=0,ncol=1, prop={'family':'serif', 'size':6,'weight':'bold'},bbox_to_anchor=(0.03,0.85,0.5,0.2))

plt.tight_layout()
plt.savefig('normalized_speedup_graphs.eps', format='eps',bbox_inches = 'tight')

