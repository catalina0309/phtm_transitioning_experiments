import re
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import statistics

def get_time2_data(filename):
	# open and read file
	log_data = open(filename).read()
	
	# get all times from the same type: app,input,thread,
	timers = []
	timer_regex = re.compile(r'Time\s*=\s*(\d*.\d*)')
	timers = [float(i) for i in re.findall(timer_regex, log_data)]
	
	return timers


# run for all apps 
_APPS_TM = ["genome","intruder","vacation","ssca2","kmeans","labyrinth"]
_INPUTS = ["2","4","8","12","18"]
_THREADS = ["18"]#["1","2","4","8","12","18"]

speedup_phtm_old = {}
speedup_phtm_new = {}
speedup_phtm_gotm = {}

speedup_phtm_old_value = []
speedup_phtm_new_value = []
speedup_phtm_gotm_value = []

speedup_phtm_old_std = []
speedup_phtm_new_std = []
speedup_phtm_gotm_std = []

speedup_rh_norec = {}
speedup_HyTM_NOrec = {}
speedup_HyCo = {}

speedup_rh_norec_value = []
speedup_HyTM_NOrec_value = []
speedup_HyCo_value = []

speedup_rh_norec_std = []
speedup_HyTM_NOrec_std = []
speedup_HyCo_std = []

results_seq = []

degree_xlabel = ["2","4","8","12","18"]
apps_ylabel =  ["genome","intruder","vacation","ssca2","kmeans","labyrinth"]
r_cols = 2
r_rows = int(len(_APPS_TM) / r_cols)
f, (p) = plt.subplots(r_rows, r_cols, figsize=[8,6])
col_ind = 0
row_ind = 0
n_runs = 5

for apps in _APPS_TM:
	filename = "../PhTMs/phtms-experiments/results/logs/"+apps+"-seq-x86_64-ptmalloc-1.log"
	temp_seq = list(get_time_data2(filename))
	results_seq.append(sum(temp_seq) / len(temp_seq))


	
for apps in range(0,len(_APPS_TM)):
	results_speedup_phtm_old = []
	results_speedup_phtm_new = []
	results_speedup_phtm_gotm = []
	
	std_speedup_phtm_old = []
	std_speedup_phtm_new = []
	std_speedup_phtm_gotm = []
	
	for inputs in _INPUTS:
		for threads in _THREADS:
			#for GoTM
			filename = "../GoTM/gotm-tm-experiments/tm/results/logs/"+_APPS_TM[apps]+"-gcctm_x86_64-phasedTM_rtm-OPTIMIZED-norec-ptmalloc-"+inputs+".log"
			temp = get_time2_data(filename)
			temp2 = results_seq[apps] / (sum(list(temp))/len(temp))
			results_speedup_phtm_gotm.append(temp2)	
			temp3 = []
			for v in temp:
				temp3.append(results_seq[apps] / v)
			std_speedup_phtm_gotm.append(statistics.stdev(temp3))				
			
			#for CTM
			filename = "../CTM/ctm-experiments/results/logs/"+_APPS_TM[apps]+"-gcctm_x86_64-phasedTM_rtm-OPTIMIZED-norec-ptmalloc-"+inputs+".log"
			temp = get_time2_data(filename)
			temp2 = results_seq[apps] / (sum(list(temp))/len(temp))
			results_speedup_phtm_new.append(temp2)
			temp3 = []
			for v in temp:
				temp3.append(results_seq[apps] / v)
			std_speedup_phtm_new.append(statistics.stdev(temp3))
			
			#for PhTM
			filename = "../PhTMs/phtms-experiments/results/logs/"+_APPS_TM[apps]+"-gcctm_x86_64-phasedTM_rtm-OPTIMIZED-norec-ptmalloc-"+inputs+".log"
			temp = get_time2_data(filename)
			temp2 = results_seq[apps] / (sum(list(temp))/len(temp))
			results_speedup_phtm_old.append(temp2)	
			temp3 = []
			for v in temp:
				temp3.append(results_seq[apps] / v)
			std_speedup_phtm_old.append(statistics.stdev(temp3))
			
			
	
	speedup_phtm_old[apps] = pd.DataFrame(np.c_[results_speedup_phtm_old[0],results_speedup_phtm_old[1],results_speedup_phtm_old[2],results_speedup_phtm_old[3],results_speedup_phtm_old[4]], columns=degree_xlabel)
	speedup_phtm_old_value.append(speedup_phtm_old[apps].mean())
	speedup_phtm_old_std.append(speedup_phtm_old[apps].std())
	print(str(apps) + " PhTM*")
	print(speedup_phtm_old[apps])
	speedup_phtm_new[apps] = pd.DataFrame(np.c_[results_speedup_phtm_new[0],results_speedup_phtm_new[1],results_speedup_phtm_new[2],results_speedup_phtm_new[3],results_speedup_phtm_new[4]], columns=degree_xlabel)
	speedup_phtm_new_value.append(speedup_phtm_new[apps].mean())
	speedup_phtm_new_std.append(speedup_phtm_old[apps].std())
	print(str(apps) + " CTTM")
	print(speedup_phtm_new[apps])
	speedup_phtm_gotm[apps] = pd.DataFrame(np.c_[results_speedup_phtm_gotm[0],results_speedup_phtm_gotm[1],results_speedup_phtm_gotm[2],results_speedup_phtm_gotm[3],results_speedup_phtm_gotm[4]], columns=degree_xlabel)
	speedup_phtm_gotm_value.append(speedup_phtm_gotm[apps].mean())
	speedup_phtm_gotm_std.append(speedup_phtm_old[apps].std())
	print(str(apps) + " gotm")
	print(speedup_phtm_gotm[apps])
	
	
	ind = len(_INPUTS)
	offset = 0.2
	width = 0.1
	p1 = p[row_ind][col_ind].bar(np.arange(-2*width,ind-2*width),speedup_phtm_new_value[apps], width=width, color="tomato",edgecolor='k')
	p2 = p[row_ind][col_ind].bar(np.arange(-width,ind-width),speedup_phtm_gotm_value[apps], width=width, color="blue",edgecolor='k')
	p3 = p[row_ind][col_ind].bar(np.arange(0,ind),speedup_phtm_old_value[apps], width=width, color="green",edgecolor='k')
	
	p[row_ind][col_ind].set_xticks(range(len(speedup_phtm_new[apps].columns)))
	p[row_ind][col_ind].set_xticklabels(speedup_phtm_new[apps].columns, fontsize=8, rotation=0)
	p[row_ind][col_ind].tick_params(axis="y", labelsize=6)
	pad = 1
	a = apps_ylabel[apps]
	p[row_ind][col_ind].set_ylabel('Normalized\nSpeedup', fontsize=8,fontname='serif')
	p[row_ind][col_ind].set_title(_APPS_TM[apps], fontsize=8,fontweight='bold',fontname='serif')
	p[row_ind][col_ind].set_xlabel('Threads', fontsize=8,fontname='serif')
	p[row_ind][col_ind].axhline(y=1,linewidth=0.3, color='k', linestyle='--')
	row_ind = row_ind + 1
	if row_ind == r_rows:
		col_ind = col_ind + 1
		row_ind = 0


f.legend((p1[0],p2[0],p3[0]), ('CTM', "GoTM",'PhTM*'),ncol=1,bbox_to_anchor=(0.03,0.9,0.5,0.2),loc="upper center",mode="expand", borderaxespad=0,frameon=False, prop={'family':'serif', 'size':8,'weight':'bold'}, labelspacing=0)

plt.tight_layout()
plt.savefig('normalized_speedup_stamp.eps', format='eps',bbox_inches = 'tight')
#plt.show()
