#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"
#include <utility>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <fstream>
#include "galois/graphs/BufferedGraph.h"

const unsigned int LABEL_INF = std::numeric_limits<unsigned int>::max();
using component_type = unsigned int;

struct LNode {
    component_type comp_current;
  };

using Graph =  galois::graphs::LC_CSR_Graph<LNode, void>::with_no_lockable<true>::type::with_numa_alloc<true>::type;
using GNode = Graph::GraphNode;

const int CHUNK_SIZE = 32;
using PSchunk = galois::worklists::ChunkFIFO<CHUNK_SIZE>;

void LabelPropAlgo (Graph& graph) {

      galois::for_each(
          galois::iterate(graph.begin(),graph.end()),
          [&](const GNode& src, auto& ctx) {
__transaction_atomic{
            LNode& sdata = graph.getData(src);
              auto begin = graph.edge_begin(src);
              auto end = graph.edge_end(src);
              while(begin != end){
                GNode dst              = graph.getEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (ddata.comp_current > sdata.comp_current)
                {
                        ddata.comp_current = sdata.comp_current;
                        ctx.push(dst);
                }
                begin++;
              }
}
          },
          galois::no_conflicts(),
          galois::wl<PSchunk>(),
          galois::loopname("LabelPropAlgo"));
}


int main(int argc, char** argv)
{
       galois::SharedMemSys G;
        Graph graph;

        char* filename = argv[1];
        std::cout << "reading Graph " << filename << "\n";
        
        std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
        int numThreads = atoi(argv[2]);
        std::cout << "numThreads: " << numThreads << std::endl;
        galois::setActiveThreads(numThreads);

	galois::graphs::BufferedGraph<void> fileReader;
	  fileReader.loadGraph(filename);
	uint32_t n_nodes = fileReader.size();
	uint64_t n_edges = fileReader.sizeEdges()*2;
	std::cout << n_nodes << " and " << n_edges << std::endl;
	  graph.allocateFrom(n_nodes,n_edges);
	  graph.constructNodes();
	  galois::do_all(galois::iterate((uint32_t)0, fileReader.size()),
		           [&](uint32_t i) {
                   auto b = fileReader.edgeBegin(i);
                   auto e = fileReader.edgeEnd(i);

                   graph.fixEndEdge(i, *e);

                   while (b < e) {
                     graph.constructEdge(*b, fileReader.edgeDestination(*b));
                     graph.constructEdge(fileReader.edgeDestination(*b), *b);
                     b++;
                   }
                 });
                 
        int id = 0;
        for (auto ii = graph.begin(), ei = graph.end(); ii != ei; ++ii)
        {
                graph.getData(*ii).comp_current = id;
                id++;
        }
  std::cout <<"starting label propagation..." << std::endl;
  auto start = std::chrono::high_resolution_clock::now();
  LabelPropAlgo(graph);
  auto finish = std::chrono::high_resolution_clock::now();

 
 id = 0;
    for(auto ii : graph)
    {
      std::cout << id << " " << graph.getData(ii).comp_current << std::endl;
      id++;
      if (id == 10)
              break;
    }

  std::chrono::duration<double> elapsed = finish - start;
        std::cout << "Elapsed time: " << elapsed.count() << "\n";
        return 0;

}

