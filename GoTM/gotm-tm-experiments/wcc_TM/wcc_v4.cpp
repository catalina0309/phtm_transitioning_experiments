#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"
#include <utility>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <fstream>

const unsigned int LABEL_INF = std::numeric_limits<unsigned int>::max();
using component_type = unsigned int;

struct LNode {
    component_type comp_current;
//    component_type comp_old;
  };

using Graph =  galois::graphs::LC_Linear_Graph<LNode, void>::with_no_lockable<true>::type::with_numa_alloc<true>::type;
using GNode = Graph::GraphNode;

const int CHUNK_SIZE = 32;
using PSchunk = galois::worklists::ChunkFIFO<CHUNK_SIZE>;

void LabelPropAlgo (Graph& graph) {

      galois::for_each(
          galois::iterate(graph),
          [&](const GNode& src, auto& ctx) {
__transaction_atomic{
            LNode& sdata = graph.getData(src);
            //std::cout << "srs old " << sdata.comp_old << " current " << sdata.comp_current << std::endl;
//            if (sdata.comp_old > sdata.comp_current) {
              auto begin = graph.edge_begin(src);
              auto end = graph.edge_end(src);
              while(begin != end){
                GNode dst              = graph.getEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (ddata.comp_current > sdata.comp_current)
                {
                        ddata.comp_current = sdata.comp_current;
                        ctx.push(dst);
                }
                begin++;
              }
//              sdata.comp_old = sdata.comp_current;
//              ctx.push(src);
//            }
}
          },
          galois::no_conflicts(),
          galois::wl<PSchunk>(),
          galois::loopname("LabelPropAlgo"));
}


int main(int argc, char** argv)
{
       galois::SharedMemSys G;
        Graph graph;

        char* filename = argv[1];
        std::cout << "reading Graph " << filename << "\n";
        galois::graphs::readGraph(graph, filename); // argv[1] is the file name for graph
        std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
        int numThreads = atoi(argv[2]);
        std::cout << "numThreads: " << numThreads << std::endl;
        galois::setActiveThreads(numThreads); // argv[2] is # of threads
	
	char* filename_t = argv[3];
	Graph grapht;
	galois::graphs::readGraph(grapht,filename_t);
	

  // Initialize transpose vertices as individual components
        int id = 0;
        for (auto ii = graph.begin(), ei = graph.end(); ii != ei; ++ii)
        {
                graph.getData(*ii).comp_current = id;
//              graph.getData(*ii).comp_old     = LABEL_INF;
                id++;
        }
  std::cout <<"starting label propagation..." << std::endl;
  auto start = std::chrono::high_resolution_clock::now();
  LabelPropAlgo(graph);
	for (auto ib = graph.begin(),ie = graph.end(),ibt = grapht.begin(), iet = grapht.end(); ib != ie; ++ib,++ibt)
	{
		auto& label = graph.getData(*ib);
		auto& labelt = graph.getData(*ibt);
		labelt.comp_current = label.comp_current;
	}
  LabelPropAlgo(grapht);
        for (auto ib = graph.begin(),ie = graph.end(),ibt = grapht.begin(), iet = grapht.end(); ib != ie; ++ib,++ibt)
        {
		auto& label = graph.getData(*ib);
                auto& labelt = graph.getData(*ibt);
                if ( labelt.comp_current < label.comp_current)
			label.comp_current = labelt.comp_current;
        }
  LabelPropAlgo(graph);

  auto finish = std::chrono::high_resolution_clock::now();

  /////////
 id = 0;
    for(auto ii : graph)
    {
      std::cout << id << " " << graph.getData(ii).comp_current << std::endl;
      id++;
      if (id == 10)
              break;
    }

  std::chrono::duration<double> elapsed = finish - start;
        std::cout << "Elapsed time: " << elapsed.count() << "\n";
        return 0;

}

