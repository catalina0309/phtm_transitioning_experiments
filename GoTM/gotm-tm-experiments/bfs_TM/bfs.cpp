#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <omp.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"
#include "galois/Timer.h"
#include <iostream>
#include <chrono>
#include <vector>


#ifdef TM
using Graph = galois::graphs::LC_Linear_Graph<int, int>::with_no_lockable<true>::type::with_numa_alloc<true>::type;
#else
using Graph = galois::graphs::LC_Linear_Graph<int, int>::with_no_lockable<true>::type;
#endif

using GNode = Graph::GraphNode;
using namespace galois::worklists;
const int CHUNK_SIZE = 32;
int MAX_BLOCK_SIZE = 1;
using PSchunk = galois::worklists::ChunkFIFO<CHUNK_SIZE>;

////
static const char *filename = "../input_graphs/rand100k00001.gr"; //r4-2e23.gr";
int numThreads = 1;

using namespace galois::worklists;

int main(int argc, char* argv[])
{
  galois::SharedMemSys G;

  Graph graph;
  filename = argv[1];
  std::cout << "reading Graph " << filename << "\n";
  galois::graphs::readGraph(graph, filename); // argv[1] is the file name for graph
   std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  numThreads = atoi(argv[2]);
   std::cout << "numThreads: " << numThreads << std::endl;

  galois::setActiveThreads(numThreads); // argv[2] is # of threads
  
  unsigned nnodes = graph.size();
  uint64_t nedges = graph.sizeEdges();
  std::cout <<"Num nodes is " << nnodes <<", num edges is " << nedges << std::endl;

  galois::preAlloc(numThreads +
                   (3 * graph.size() * sizeof(typename Graph::node_data_type)) /
                       galois::runtime::pagePoolSize());
  //galois::reportPageAlloc("MeminfoPre");
  // initialization
  
  srand (1234);
  galois::do_all(galois::iterate(graph),
                 [&graph](GNode N) {
                  auto& sdata =  graph.getData(N);
                  sdata = 1410065408;
                 }
  );
  for (auto N : graph)
  {
                 for (auto ii : graph.edges(N))
                 {
                   auto& weight   = graph.getEdgeData(ii);
                   weight = rand() % 10 + 1;
                 }

  }
  
  ////
  int start_node = 0;
  if (argv[3] != NULL)
	  start_node = atoi(argv[3]);

 
  //operator for loop
   auto BFS = [&](GNode active_node, auto& ctx) {
//int src_nout = std::distance(graph.edge_begin(active_node),graph.edge_end(active_node));
 
//std::cout << src_nout << std::endl;
#ifdef TM
    __transaction_atomic{
#endif

    		auto srcData = graph.getData(active_node);
		auto begin = active_node->edgeBegin();
		auto end = active_node->edgeEnd();
    		while(begin != end) { 
      			auto dst      = graph.getEdgeDst(begin);
      			auto& dstData = graph.getData(dst);
      			if (dstData > (srcData + 1)) {
        			dstData = srcData + 1;
        			ctx.push(dst); // add new work items
			}
			begin++;
		}

#ifdef TM
	}
#endif
  };

  ////
  std::cout << "starting process\n";
 //for each loops
 auto start = std::chrono::high_resolution_clock::now();
 int ID = 0;
 for (auto ii : graph)
 {
	 if(ID == start_node)
	 {
		//int src_nout =  std::distance(graph.edge_begin(ii), graph.edge_end(ii));
		//std::cout << "edges: " << src_nout << std::endl;
		// clear source
  		graph.getData(ii) = 0;
 #ifdef TM
 	 	galois::for_each(galois::iterate({ii}), BFS, galois::wl<PSchunk>(),galois::no_conflicts());
 #else
  		galois::for_each(galois::iterate({ii}), BFS, galois::wl<PSchunk>());
 #endif
	 }
	ID++;
 }
 auto finish = std::chrono::high_resolution_clock::now();
 ////


//printing node's path cost
 int iter = 0;
 for (auto ii : graph)
 {
   std::cout<< graph.getData(ii) << std::endl;
   if (iter == 20)
           break;
   iter++;
 }

 std::cout << "Version: ";
#ifdef TM
 std::cout << "TM" << std::endl;
#else
 std::cout << "GALOIS LOCKING" << std::endl;
#endif
 std::chrono::duration<double> elapsed = finish - start;
 std::cout << "Elapsed time: " << elapsed.count() << "\n";

  return 0;
}

