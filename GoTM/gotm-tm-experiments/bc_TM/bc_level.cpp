constexpr static const char* const REGION_NAME = "BC";
#include <limits>
#include <fstream>
#include "galois/gstl.h"
#include "galois/Reduction.h"
#include "galois/Timer.h"
#include "galois/AtomicHelpers.h"
#include "galois/graphs/LCGraph.h"
#include <chrono>

// type of the num shortest paths variable
using ShortPathType = double;

// Graph structure declarations
const uint32_t infinity        = std::numeric_limits<uint32_t>::max() / 4;
static uint64_t currentSrcNode = 0;

// NOTE: types assume that these values will not reach uint64_t: it may
// need to be changed for very large graphs
struct NodeData {
  uint32_t currentDistance;
  ShortPathType numShortestPaths;
  float dependency;
  float bc;
};

// reading in list of sources to operate on if provided
std::ifstream sourceFile;
std::vector<uint64_t> sourceVector;

using Graph = galois::graphs::LC_Linear_Graph<NodeData, void>::with_no_lockable<true>::type::with_numa_alloc<true>::type;
using GNode = Graph::GraphNode;
using WorklistType = galois::InsertBag<GNode, 4096>;

constexpr static const unsigned CHUNK_SIZE = 256u;

//Functions for running the algorithm 
 // Initialize node fields all to 0
 // @param graph Graph to initialize
 
void InitializeGraph(Graph& graph) {
  galois::do_all(
    galois::iterate(graph),
    [&] (GNode n) {
      NodeData& nodeData = graph.getData(n);
      nodeData.currentDistance    = 0;
      nodeData.numShortestPaths   = 0;
      nodeData.dependency         = 0;
      nodeData.bc                 = 0;
    },
    //galois::no_stats(),
    galois::loopname("InitializeGraph")
  );
}


 // Resets data associated to start a new SSSP with a new source.
 //
 // @param graph Graph to reset iteration data
 
void InitializeIteration(Graph& graph, GNode currentSrcN) {
  galois::do_all(
    galois::iterate(graph),
    [&] (GNode n) {
      NodeData& nodeData = graph.getData(n);
      bool isSource = (n == currentSrcN);
      // source nodes have distance 0 and initialize short paths to 1, else
      // distance is infinity with 0 short paths
      if (!isSource) {
        nodeData.currentDistance    = infinity;
        nodeData.numShortestPaths   = 0;
      } else {
        nodeData.currentDistance    = 0;
        nodeData.numShortestPaths   = 1;
      }
      // dependency reset for new source
      nodeData.dependency         = 0;
    },
    //galois::no_stats(),
    galois::loopname("InitializeIteration")
  );
};


 // Forward phase: SSSP to determine DAG and get shortest path counts.
 //
 // Worklist-based push. Save worklists on a stack for reuse in backward
 // Brandes dependency propagation.
 
galois::gstl::Vector<WorklistType> SSSP(Graph& graph, GNode currentSrcN) {
  galois::gstl::Vector<WorklistType> stackOfWorklists;
  uint32_t currentLevel = 0;

  // construct first level worklist which consists only of source
  stackOfWorklists.emplace_back();
  stackOfWorklists[0].emplace(currentSrcN);

  // loop as long as current level's worklist is non-empty
  while (!stackOfWorklists[currentLevel].empty()) {
    // create worklist for next level
    stackOfWorklists.emplace_back();
    uint32_t nextLevel = currentLevel + 1;

    galois::for_each(
      galois::iterate(stackOfWorklists[currentLevel]),
      [&] (GNode n,  auto& ctx) {
#ifdef TM
      __transaction_atomic{
#endif
        NodeData& curData = graph.getData(n);
	auto begin = n->edgeBegin();
	auto end = n-> edgeEnd();
        while(begin != end)
	{//for (auto e : graph.edges(n)) {
          GNode dest = graph.getEdgeDst(begin);
          NodeData& destData = graph.getData(dest);

          if (destData.currentDistance == infinity) {
            uint32_t oldVal;
            //here I changed this //uint32_t oldVal =__sync_val_compare_and_swap(&(destData.currentDistance), infinity, nextLevel); for the following
            if (destData.currentDistance == infinity) {
              oldVal =  destData.currentDistance;
              destData.currentDistance = nextLevel;
            }
            ////
            // only 1 thread should add to worklist
            if (oldVal == infinity) {
              stackOfWorklists[nextLevel].emplace(dest);
            }

            destData.numShortestPaths = destData.numShortestPaths + curData.numShortestPaths;
            //galois::atomicAdd(destData.numShortestPaths, curData.numShortestPaths.load());
          } else if (destData.currentDistance == nextLevel) {
            destData.numShortestPaths = destData.numShortestPaths + curData.numShortestPaths;
            //galois::atomicAdd(destData.numShortestPaths, curData.numShortestPaths.load());
          }
	  *begin++;
        }
#ifdef TM
        }
#endif
      },
      galois::no_conflicts(),
      galois::no_pushes(),
      galois::chunk_size<CHUNK_SIZE>(),
      galois::loopname("SSSP")
    );

    // move on to next level
    currentLevel++;
  }
  return stackOfWorklists;
}


 // Backward phase: use worklist of nodes at each level to back-propagate
 // dependency values.
 //
 // @param graph Graph to do backward Brandes dependency prop on
 
void BackwardBrandes(Graph& graph,
                     galois::gstl::Vector<WorklistType>& stackOfWorklists) {
  // minus 3 because last one is empty, one after is leaf nodes, and one
  // to correct indexing to 0 index
  if (stackOfWorklists.size() >= 3) {
    uint32_t currentLevel = stackOfWorklists.size() - 3;

    // last level is ignored since it's just the source
    while (currentLevel > 0) {
      WorklistType& currentWorklist = stackOfWorklists[currentLevel];
      uint32_t succLevel = currentLevel + 1;

      galois::for_each(
        galois::iterate(currentWorklist),
        [&] (GNode n, auto& ctx) {
          NodeData& curData = graph.getData(n);
          GALOIS_ASSERT(curData.currentDistance == currentLevel);

          for (auto e : graph.edges(n)) {
            GNode dest = graph.getEdgeDst(e);
            NodeData& destData = graph.getData(dest);

            if (destData.currentDistance == succLevel) {
              // grab dependency, add to self
              float contrib = ((float)1 + destData.dependency) /
                              destData.numShortestPaths;
              curData.dependency = curData.dependency + contrib;
            }
          }

          // multiply at end to get final dependency value
          curData.dependency *= curData.numShortestPaths;
          // accumulate dependency into bc
          curData.bc += curData.dependency;
        },
        galois::no_conflicts(),
        galois::no_pushes(),
        galois::chunk_size<CHUNK_SIZE>(),
        galois::loopname("Brandes")
      );

      // move on to next level lower
      currentLevel--;
    }
  }
}


// Sanity check 
 // Get some sanity numbers (max, min, sum of BC)
 //
 // @param graph Graph to sanity check
 
void Sanity(Graph& graph) {
  galois::GReduceMax<float> accumMax;
  galois::GReduceMin<float> accumMin;
  galois::GAccumulator<float> accumSum;
  accumMax.reset();
  accumMin.reset();
  accumSum.reset();

  // get max, min, sum of BC values using accumulators and reducers
  galois::do_all(
    galois::iterate(graph),
    [&] (GNode n) {
      NodeData& nodeData = graph.getData(n);
      accumMax.update(nodeData.bc);
      accumMin.update(nodeData.bc);
      accumSum += nodeData.bc;
    },
    //galois::no_stats(),
    galois::loopname("Sanity")
  );

  galois::gPrint("Max BC is ", accumMax.reduce(), "\n");
//  galois::gPrint("Min BC is ", accumMin.reduce(), "\n");
  galois::gPrint("BC sum is ", accumSum.reduce(), "\n");
}


// Main method for running 
int main(int argc, char** argv) {
  galois::SharedMemSys G;
  char* filename = argv[1];
  std::cout << "reading Graph " << filename << "\n";
  //galois::graphs::readGraph(graph, filename); // argv[1] is the file name for graph
  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int numThreads = atoi(argv[2]);
  std::cout << "numThreads: " << numThreads << std::endl;
  galois::setActiveThreads(numThreads); // argv[2] is # of threads
  uint32_t numberOfSources = 1;
  int start_node = 0;
  if (argv[3] != NULL)
  {
    start_node = atoi(argv[3]);
  }
  // some initial stat reporting
  galois::StatTimer totalTimer("TimerTotal", REGION_NAME);
  totalTimer.start();

  // Graph construction
  galois::StatTimer graphConstructTimer("TimerConstructGraph", "BFS");
  graphConstructTimer.start();
  Graph graph;
  galois::graphs::readGraph(graph, filename);
  graphConstructTimer.stop();
  galois::gInfo("Graph construction complete");

  // determine how many sources to loop over based on command line args
  uint64_t loop_end = numberOfSources;
  // graph initialization, then main loop
  InitializeGraph(graph);

  galois::gInfo("Beginning main computation");
  galois::StatTimer runtimeTimer;
  auto start = std::chrono::high_resolution_clock::now();
  // loop over all specified sources for SSSP/Brandes calculation
  int ID = 0;
  for (auto ii : graph){
    // worklist; last one will be empty
    if (ID == start_node)
    {
	InitializeIteration(graph, ii);
    	galois::gstl::Vector<WorklistType> worklists = SSSP(graph, ii);
    	BackwardBrandes(graph, worklists);
	break;
    }
    ID++;
  }
  totalTimer.stop();
  auto finish = std::chrono::high_resolution_clock::now();
  
  std::chrono::duration<double> elapsed = finish - start;
  std::cout << "Elapsed time: " << elapsed.count() << "\n";
  //galois::reportPageAlloc("MemAllocPost");

  // sanity checking numbers
  Sanity(graph);

  /* // Verify, i.e. print out graph data for examination
  int verify = 1;
  if (verify) {
    char* v_out = (char*)malloc(40);
    for (auto ii = graph.begin(); ii != graph.end(); ++ii) {
      // outputs betweenness centrality
      sprintf(v_out, "%u %.9f\n", (*ii), graph.getData(*ii).bc);
      galois::gPrint(v_out);
    }
    free(v_out);
  }*/

  return 0;
}
