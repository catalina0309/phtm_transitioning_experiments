#include "galois/Galois.h"
#include "galois/gstl.h"
#include "galois/Reduction.h"
#include "galois/AtomicHelpers.h"
#include "galois/graphs/LCGraph.h"

#include <chrono>

const int CHUNK_SIZE = 32;

struct NodeData {
#ifdef TM
  uint32_t currentDegree;
#else
  std::atomic<uint32_t> currentDegree;
#endif
};


using Graph = galois::graphs::LC_Linear_Graph<NodeData, void>::with_no_lockable<true>::type;

using GNode = Graph::GraphNode;


void degreeCounting(Graph& graph) {

 for (auto node : graph )
   {
          auto& nodeData = graph.getData(node);
          nodeData.currentDegree = 0;
   }

  for (auto node : graph )
  {
        for (auto e : graph.edges(node))
        {
                 GNode dst = graph.getEdgeDst(e);
                 auto& dstData = graph.getData(dst);
                 dstData.currentDegree ++;
        }
  }
};


void setupInitialWorklist(Graph& graph,
                          galois::InsertBag<GNode>& initialWorklist,  unsigned int k_core_num) {
  galois::do_all(
    galois::iterate(graph.begin(), graph.end()),
    [&] (GNode curNode) {
      NodeData& curData = graph.getData(curNode);
      if (curData.currentDegree < k_core_num) {
        
        initialWorklist.emplace(curNode);
      }
    },
    galois::loopname("InitialWorklistSetup"),
    galois::no_stats()
  );
}

void asyncCascadeKCore(Graph& graph, galois::InsertBag<GNode>& initialWorklist,  unsigned int k_core_num) {
  galois::for_each(
    galois::iterate(initialWorklist),
    [&] (GNode deadNode, auto& ctx) {
    
#ifdef TM
      __transaction_atomic{
#endif
      auto begin = deadNode->edgeBegin();
      auto end = deadNode->edgeEnd();
      while (begin != end)
      {
        GNode dest = graph.getEdgeDst(begin);
        NodeData& destData = graph.getData(dest);
        destData.currentDegree = destData.currentDegree - 1u;
        if (destData.currentDegree == k_core_num)
        {
         
          ctx.push(dest);
        }
        *begin++;
      }

#ifdef TM
      }
#endif
    },
    galois::wl<galois::worklists::ChunkFIFO<CHUNK_SIZE>>(),
    galois::no_conflicts(),
    galois::loopname("AsyncCascadeDeadNodes")
  );
}


int main(int argc, char** argv) {
  galois::SharedMemSys G;

  Graph graph;
  galois::graphs::readGraph(graph, argv[1]);
  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int NumThreads = atoi(argv[2]);
  std::cout << "NumThreads: " << NumThreads << std::endl;
  galois::setActiveThreads(NumThreads);

   
  unsigned int k_core_num = (graph.sizeEdges() / graph.size());
  std::cout << "k_core_num: " << k_core_num << std::endl;

  galois::StatTimer runtimeTimer;

    runtimeTimer.start();


    galois::InsertBag<GNode> initialWorklist;
    setupInitialWorklist(graph, initialWorklist, k_core_num);

    std::cout << "performing comp...\n";
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = k_core_num; i < (k_core_num+5); i++)
    {
		degreeCounting(graph);
		galois::InsertBag<GNode> initialWorklist;
		setupInitialWorklist(graph, initialWorklist, i);
	    	asyncCascadeKCore(graph, initialWorklist, i);
	}
    auto finish = std::chrono::high_resolution_clock::now();

    runtimeTimer.stop();


    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << "\n";
  return 0;
}

