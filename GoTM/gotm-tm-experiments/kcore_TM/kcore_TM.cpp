/*
 * This file belongs to the Galois project, a C++ library for exploiting parallelism.
 * The code is being released under the terms of the 3-Clause BSD License (a
 * copy is located in LICENSE.txt at the top-level directory).
 *
 * Copyright (C) 2019, The University of Texas at Austin. All rights reserved.
 * UNIVERSITY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES CONCERNING THIS
 * SOFTWARE AND DOCUMENTATION, INCLUDING ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR ANY PARTICULAR PURPOSE, NON-INFRINGEMENT AND WARRANTIES OF
 * PERFORMANCE, AND ANY WARRANTY THAT MIGHT OTHERWISE ARISE FROM COURSE OF
 * DEALING OR USAGE OF TRADE.  NO WARRANTY IS EITHER EXPRESS OR IMPLIED WITH
 * RESPECT TO THE USE OF THE SOFTWARE OR DOCUMENTATION. Under no circumstances
 * shall University be liable for incidental, special, indirect, direct or
 * consequential damages or loss of profits, interruption of business, or
 * related expenses which may arise from use of Software or Documentation,
 * including but not limited to those resulting from defects in Software and/or
 * Documentation, or loss or inaccuracy of data of any kind.
 */

#include "galois/Galois.h"
#include "galois/gstl.h"
#include "galois/Reduction.h"
#include "galois/AtomicHelpers.h"
#include "galois/graphs/LCGraph.h"

#include <chrono>

const int CHUNK_SIZE = 32;

// Node deadness can be derived from current degree and k value, so no field
// necessary
struct NodeData {

  uint32_t currentDegree;//std:atomic<uint32_t> currentDegree;

};

//! Typedef for graph used, CSR graph
using Graph = galois::graphs::LC_Linear_Graph<NodeData, void>::with_no_lockable<true>::type;
//! Typedef for node type in the CSR graph
using GNode = Graph::GraphNode;


// Initialize degree fields in graph with current degree. Since symmetric,
// out edge count is equivalent to in-edge count.
//
// @param graph Graph to initialize degrees in
//
void degreeCounting(Graph& graph) {
  
 for (auto node : graph )
   {
          auto& nodeData = graph.getData(node);
          nodeData.currentDegree = 0;
   }

  for (auto node : graph )
  {
        for (auto e : graph.edges(node))
        {
		 GNode dst = graph.getEdgeDst(e);
                 auto& dstData = graph.getData(dst);
                 dstData.currentDegree ++;
	}  
  }
};

//
// Setup initial worklist of dead nodes.
//
// @param graph Graph to operate on
// @param initialWorklist Empty worklist to be filled with dead nodes.
//
void setupInitialWorklist(Graph& graph,
                          galois::InsertBag<GNode>& initialWorklist,  unsigned int k_core_num) {
  galois::do_all(
    galois::iterate(graph.begin(), graph.end()),
    [&] (GNode curNode) {
      NodeData& curData = graph.getData(curNode);
      if (curData.currentDegree < k_core_num) {
        // dead node, add to initialWorklist for processing later
        initialWorklist.emplace(curNode);
      }
    },
//    galois::loopname("InitialWorklistSetup"),
    galois::no_stats()
  );
}

// Starting with initial dead nodes, degree decrement and add to worklist
// as they drop below k threshold until worklist is empty (i.e. no more dead
// nodes).
//
// @param graph Graph to operate on
// @param initialWorklist Worklist containing initial dead nodes
//
void asyncCascadeKCore(Graph& graph, galois::InsertBag<GNode>& initialWorklist,  unsigned int k_core_num) {
  galois::for_each(
    galois::iterate(initialWorklist),
    [&] (GNode deadNode, auto& ctx) {
      // decrement degree of all neighbors
#ifdef TM
      __transaction_atomic{
#endif
      auto begin = deadNode->edgeBegin();
      auto end = deadNode->edgeEnd();
      while (begin != end)//for (auto e : graph.edges(deadNode)) 
      {
        GNode dest = graph.getEdgeDst(begin);
        NodeData& destData = graph.getData(dest);
        destData.currentDegree = destData.currentDegree - 1u;
	if (destData.currentDegree == (k_core_num)) 
	{
          // this thread was responsible for putting degree of destination
          // below threshold: add to worklist
          ctx.push(dest);
        }
	*begin++;
      }

#ifdef TM
      }
#endif
    },
    galois::wl<galois::worklists::ChunkFIFO<CHUNK_SIZE>>(),
    galois::no_conflicts(),
    galois::no_stats()
//    galois::loopname("AsyncCascadeDeadNodes")
  );
}

// Main method for running 
int main(int argc, char** argv) {
  galois::SharedMemSys G;

  Graph graph;
  galois::graphs::readGraph(graph, argv[1]);
  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int NumThreads = atoi(argv[2]);
  std::cout << "NumThreads: " << NumThreads << std::endl;
  galois::setActiveThreads(NumThreads);// Galois will cap at hw max
  
    // some initial stat reporting
  unsigned int k_core_num = 2;//(graph.sizeEdges() / graph.size())*2;
if (argv[3] != NULL)
	k_core_num = atoi(argv[3]);
std::cout << "k_core_num: " << k_core_num << std::endl;
  // preallocate pages in memory so allocation doesn't occur during compute
  
//  galois::preAlloc(std::max(
//    (uint64_t)galois::getActiveThreads() * (graph.size() / 1000000),
//    std::max(10u, galois::getActiveThreads()) * (size_t)10
//  ));
//  galois::reportPageAlloc("MemAllocMid");

  // intialization of degrees
  degreeCounting(graph);

  // here begins main computation
//  galois::StatTimer runtimeTimer;

  //  runtimeTimer.start();


    // worklist setup of initial dead ndoes
    galois::InsertBag<GNode> initialWorklist;
    setupInitialWorklist(graph, initialWorklist, k_core_num);
    // actual work; propagate deadness by decrementing degrees and adding dead
    // nodes to worklist
    std::cout << "performing comp...\n";
    auto start = std::chrono::high_resolution_clock::now();

    asyncCascadeKCore(graph, initialWorklist, k_core_num);
    
    auto finish = std::chrono::high_resolution_clock::now();
    
//    runtimeTimer.stop();


    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << "\n";
  return 0;
}
