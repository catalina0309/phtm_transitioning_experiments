#include "galois/Bag.h"
#include "galois/Galois.h"
#include "galois/Timer.h"
#include "galois/graphs/LCGraph.h"
#include "galois/graphs/TypeTraits.h"

#include <chrono>


// These implementations are based on the Push-based PageRank computation
// (Algorithm 4) as described in the PageRank Europar 2015 paper.

constexpr static const float ALPHA         = 0.85;
constexpr static const float INIT_RESIDUAL = 1 - ALPHA;

constexpr static const float TOLERANCE   = 0.01;//1.0e-2;
constexpr static const unsigned MAX_ITER = 10000;

constexpr static const unsigned PRINT_TOP = 20;

constexpr static const unsigned CHUNK_SIZE = 32;

// Type definitions
typedef float PRTy;
float tolerance = TOLERANCE;
float maxIterations = MAX_ITER;

struct LNode {
  PRTy value;
  PRTy residual;

  void init() {
    value    = 0.0;
    residual = INIT_RESIDUAL;
  }

  friend std::ostream& operator<<(std::ostream& os, const LNode& n) {
    os << "{PR " << n.value << ", residual " << n.residual << "}";
    return os;
  }
};

typedef galois::graphs::LC_Linear_Graph<LNode, void> ::with_no_lockable<true>::type Graph;
typedef typename Graph::GraphNode GNode;

template <typename GNode>
struct TopPair {
  float value;
  GNode id;

  TopPair(float v, GNode i) : value(v), id(i) {}

  bool operator<(const TopPair& b) const {
    if (value == b.value)
      return id > b.id;
    return value < b.value;
  }
};

template <typename Graph>
void printTop(Graph& graph, unsigned topn = PRINT_TOP) {

  using GNode = typename Graph::GraphNode;
  typedef TopPair<GNode> Pair;
  typedef std::map<Pair, GNode> TopMap;

  TopMap top;

  for (auto ii = graph.begin(), ei = graph.end(); ii != ei; ++ii) {
    GNode src  = *ii;
    auto& n    = graph.getData(src);
    PRTy value = n.value;
    Pair key(value, src);

    if (top.size() < topn) {
      top.insert(std::make_pair(key, src));
      continue;
    }

    if (top.begin()->first < key) {
      top.erase(top.begin());
      top.insert(std::make_pair(key, src));
    }
  }

  int rank = 1;
  std::cout << "Rank PageRank Id\n";
  for (auto ii = top.rbegin(), ei = top.rend(); ii != ei; ++ii, ++rank) {
    std::cout << rank << ": " << ii->first.value << " " << ii->first.id << "\n";
  }
}

void asyncPageRank(Graph& graph) {
  typedef galois::worklists::ChunkFIFO<CHUNK_SIZE> WL;
  galois::for_each(
      galois::iterate(graph),
      [&](GNode src, auto& ctx) {
#ifdef TM
      __transaction_atomic{
#endif
        LNode& sdata = graph.getData(src);

        if (sdata.residual > tolerance) {
          PRTy oldResidual = sdata.residual;
          sdata.residual = 0;
          sdata.value = sdata.value + oldResidual;
          int src_nout = std::distance(graph.edge_begin(src),
                                       graph.edge_end(src));
          if (src_nout > 0) {
            PRTy delta = oldResidual * ALPHA / src_nout;
            // for each out-going neighbors
	    auto begin = src->edgeBegin();
	    auto end = src->edgeEnd();
            while (begin != end)
	    {
              GNode dst    = graph.getEdgeDst(begin);
              LNode& ddata = graph.getData(dst);
              if (delta > 0) {
                auto old = ddata.residual;
                ddata.residual += delta;
                if ((old < tolerance) && (old + delta >= tolerance)) {
                  ctx.push(dst);
                }
              }
	      *begin++;
            }
          }
        }
#ifdef TM 
        }
#endif
      },
      galois::loopname("PushResidualAsync"), 
      galois::no_conflicts(),
      galois::wl<WL>());
}

int main(int argc, char** argv) {
  galois::SharedMemSys G;
  
  char* filename = argv[1];
  std::cout << "reading Graph " << filename << "\n";
  //galois::graphs::readGraph(graph, filename); // argv[1] is the file name for graph
  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int numThreads = atoi(argv[2]);
  std::cout << "numThreads: " << numThreads << std::endl;
  galois::setActiveThreads(numThreads); // argv[2] is # of threads

  galois::StatTimer T("OverheadTime");
  T.start();

  Graph graph;
  galois::graphs::readGraph(graph, filename);
  std::cout << "Read " << graph.size() << " nodes, " << graph.sizeEdges() << " edges\n";

  galois::preAlloc(5 * numThreads +
                   (5 * graph.size() * sizeof(typename Graph::node_data_type)) /
                       galois::runtime::pagePoolSize());
  galois::reportPageAlloc("MeminfoPre");

  std::cout << "tolerance:" << tolerance << ", maxIterations:" << maxIterations
            << "\n";

  galois::do_all(galois::iterate(graph),
                 [&graph](GNode n) { graph.getData(n).init(); },
                 galois::no_stats(), galois::loopname("Initialize"));

  galois::StatTimer Tmain;
  Tmain.start();
  auto start = std::chrono::high_resolution_clock::now();
  
  std::cout << "Running Edge Async push version,";
  asyncPageRank(graph);
  
  auto finish = std::chrono::high_resolution_clock::now();
  Tmain.stop();

  galois::reportPageAlloc("MeminfoPost");

  printTop(graph);

  T.stop();
  
  std::chrono::duration<double> elapsed = finish - start;
  std::cout << "Elapsed time: " << elapsed.count() << "\n";

  return 0;
}
