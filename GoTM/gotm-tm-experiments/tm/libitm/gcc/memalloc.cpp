#include <stdlib.h>
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
#include "api/api.hpp"
#include "stm/txthread.hpp"
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
#include "stm.h"
#include "mod_mem.h"
#else
#error "unknown or no stm for phasedtm specified!"
#endif
#include "libitm.h"

void *_ITM_MALLOC ITM_PURE
_ITM_malloc (size_t size) {
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
	return stm::tx_alloc (size);
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
	return stm_malloc(size);
#else
#error "unknown or no stm for phasedtm specified!"
#endif
}

void *_ITM_MALLOC ITM_PURE
_ITM_calloc (size_t nmemb, size_t size) {
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
	void* ptr = stm::tx_alloc(nmemb*size);
	memset(ptr, 0, nmemb*size);
	return ptr;
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
	return stm_calloc(nmemb, size);
#else
#error "unknown or no stm for phasedtm specified!"
#endif
}

void ITM_PURE
_ITM_free (void *addr) {
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
	stm::tx_free (addr);
#elif defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
	stm_free(addr, sizeof(stm_word_t));
#else
#error "unknown or no stm for phasedtm specified!"
#endif
}
