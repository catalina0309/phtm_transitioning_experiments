#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <pthread.h>
#include "target.h"
#include "libitm.h"
#include "thread.h"
#if defined(BACKEND_NOREC) || defined(PHASEDTM_STM_NOREC)
#include "api/api.hpp"
#include "stm/txthread.hpp"
#endif
#if defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
#include "stm.h"
#endif

#if defined(BACKEND_PHASEDTM)
#include <phTM.h>
#endif

extern "C" {
uint32_t ITM_REGPARM
GTM_beginTransaction (uint32_t codeProperties, jmpbuf_t* jmpbuf);
void ITM_REGPARM GTM_rollback();
}

uint32_t ITM_REGPARM
GTM_beginTransaction (uint32_t codeProperties, jmpbuf_t* jmpbuf) {

	threadDescriptor_t* tx = getThreadDescriptor();
	if ( unlikely (tx == NULL) ) {
		tx = new threadDescriptor_t();
		initThreadDescriptor(tx);
		setThreadDescriptor(tx);
	}
	tx->codeProperties = codeProperties;
	tx->jmpbuf = *jmpbuf;
  tx->jmpbuf.rip = tx->jmpbuf.rip - 5;
  uint32_t codePathToRun = a_runInstrumentedCode;
#if defined(BACKEND_TINYSTM) || defined(PHASEDTM_STM_TINYSTM)
  int has_aborted = tx->aborted;
#endif
	tx->aborted = false;
#if defined(BACKEND_NOREC)
	// RSTM handles nesting by flattening transactions
	stm::begin((stm::TxThread*)tx->stmTxDescriptor, &tx->jmpbuf, 0);
	CFENCE;
#elif defined(BACKEND_TINYSTM)
  if (!has_aborted) {
    stm_tx_attr_t _a = {{.id = 0, .read_only = 0, .visible_reads = 0}};
    jmpbuf_t* _e = stm_start(_a, 0);
    if (_e != NULL) {
      *_e = tx->jmpbuf;
      __asm__ volatile ("":::"memory");
    }
  }
#elif defined(BACKEND_PHASEDTM)
  while(1) {
    uint64_t mode = getMode();
    if (mode == HW || mode == GLOCK) {
      bool modeChanged = HTM_Start_Tx();
      if (!modeChanged) {
        codePathToRun = a_runUninstrumentedCode;
        break;
      }
    } else { // mode == SW
      bool restarted = codeProperties != 0;
      bool modeChanged = STM_PreStart_Tx(restarted);
      if (!modeChanged) {
#if defined(PHASEDTM_STM_NOREC)
        stm::begin((stm::TxThread*)tx->stmTxDescriptor,
            &tx->jmpbuf, codeProperties);
        CFENCE;
#elif defined(PHASEDTM_STM_TINYSTM)
        if (!has_aborted) {
          stm_tx_attr_t _a = {{.id = 0, .read_only = 0, .visible_reads = 0}};
          jmpbuf_t* _e = stm_start(_a, 0);
          if (_e != NULL) {
            *_e = tx->jmpbuf;
            __asm__ volatile ("":::"memory");
          }
        }
#else
#error "unknown or no stm for phasedtm selected!"
#endif
        codePathToRun = a_runInstrumentedCode;
        break;
      }
    }
  }
#else
#error "unknown or no backend selected!"
#endif

	return codePathToRun;
}

void ITM_REGPARM
_ITM_commitTransaction () {

	threadDescriptor_t* tx = getThreadDescriptor();
#if defined(BACKEND_NOREC)
	stm::commit((stm::TxThread*)tx->stmTxDescriptor);
	CFENCE;
	tx = getThreadDescriptor();
	tx->undolog.commit();
#elif defined(BACKEND_TINYSTM)
  stm_commit();
  __asm__ volatile ("":::"memory");
  tx = getThreadDescriptor();
  tx->undolog.commit();
#elif defined(BACKEND_PHASEDTM)
	uint64_t mode = getMode();
  if (mode == SW) {
#if defined(PHASEDTM_STM_NOREC)
    stm::commit((stm::TxThread*)tx->stmTxDescriptor);
    CFENCE;
#elif defined(PHASEDTM_STM_TINYSTM)
   stm_commit();
   __asm__ volatile ("":::"memory");
#else
#error "unknown or no stm for phasedtm selected!"
#endif
    STM_PostCommit_Tx();
    tx = getThreadDescriptor();
    tx->undolog.commit();
  } else {
    // runmode == HW || GLOCK
    HTM_Commit_Tx();
  }
#else
#error "unknown or no backend selected!"
#endif
	tx->aborted = false;
}

void ITM_REGPARM
_ITM_commitTransactionEH () {
  _ITM_commitTransaction();
}

bool ITM_REGPARM
_ITM_tryCommitTransaction () {
	fprintf (stderr, "error: _ITM_tryCommitTransaction not implemented yet!\n");
	exit (EXIT_FAILURE);
	return false;
}


void _ITM_NORETURN ITM_REGPARM
_ITM_abortTransaction (_ITM_abortReason abortReason UNUSED) {
#if defined(BACKEND_NOREC)
	stm::restart();
#elif defined(BACKEND_TINYSTM)
  stm_abort(0);
#elif defined(BACKEND_PHASEDTM)
  uint64_t mode = getMode();
  if (mode == SW) {
#if defined(PHASEDTM_STM_NOREC)
    stm::restart();
#elif defined(PHASEDTM_STM_TINYSTM)
    stm_abort(0);
#else
#error "unknown or no stm for phasedtm selected!"
#endif
  } else {
    htm_abort();
  }
#else
#error "unknown or no backend selected!"
#endif
}

void ITM_REGPARM
GTM_rollback () {
#if defined(DEBUG)
	fprintf (stderr, "debug: GTM_rollback called!\n");
#endif
	threadDescriptor_t* tx = getThreadDescriptor();
	tx->aborted = true;
	tx->undolog.rollback();
}

//__attribute__((constructor(65535)))
int ITM_REGPARM _ITM_initializeProcess() {
#if 0
#if defined(BACKEND_NOREC)
  stm::sys_init(NULL);
#elif defined(BACKEND_PHASEDTM)
#else
  stm::sys_init(NULL);
  phTM_init();
#error "unknown or no backend selected!"
#endif
#endif
  return 0;
}

//__attribute__((destructor))
void ITM_REGPARM _ITM_finalizeProcess() {
#if 0
#if defined(BACKEND_NOREC)
  stm::sys_shutdown();
#elif defined(BACKEND_PHASEDTM)
#else
  phTM_term();
  stm::sys_shutdown();
#error "unknown or no backend selected!"
#endif
#endif
}

void ITM_REGPARM
_ITM_addUserCommitAction(_ITM_userCommitFunction commitFunction UNUSED, _ITM_transactionId resumingTransactionId UNUSED, void* args UNUSED) {
}
