#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <pthread.h>

#include <htm.h>
#include <aborts_profiling.h>
#include <types.h>

#define UNUSED __attribute__((unused))

#define HTM_MAX_RETRIES 9

#define MODE_CYCLES_THRESHOLD 100000000//100000000

const modeIndicator_t NULL_INDICATOR = { .value = 0 };

static __thread bool deferredTx __ALIGN__ = false;
static __thread bool decUndefCounter __ALIGN__ = false;
volatile modeIndicator_t modeIndicator	__ALIGN__ = {
																	.mode = HW,
																	.deferredCount = 0,
                                  .undeferredCount = 0
																};
volatile char padding1[__CACHE_LINE_SIZE__ - sizeof(modeIndicator_t)] __ALIGN__;

__thread uint64_t htm_retries __ALIGN__;
__thread uint32_t abort_reason __ALIGN__ = 0;
#if DESIGN == OPTIMIZED
__thread bool htm_global_lock_is_mine __ALIGN__ = false;
__thread bool has_already_transited_to_sw __ALIGN__ = false;
#define MAX_STM_RUNS 1000
#define MAX_GLOCK_RUNS 100
__thread uint64_t t0 __ALIGN__ = 0;
__thread uint64_t sum_cycles __ALIGN__ = 0;
__thread uint64_t sum_tx_cycles __ALIGN__ = 0;
__thread uint64_t sum_commits __ALIGN__ = 0;
__thread double hw_throughput __ALIGN__ = 0;
__thread double sw_throughput __ALIGN__ = 10000000000;
#define TX_CYCLES_THRESHOLD (100000)  

#if defined(__powerpc__) || defined(__ppc__) || defined(__PPC__)
static inline uint64_t getCycles()
{
		uint32_t upper, lower,tmp;
		__asm__ volatile(
			"0:                  \n"
			"\tmftbu   %0        \n"
			"\tmftb    %1        \n"
			"\tmftbu   %2        \n"
			"\tcmpw    %2,%0     \n"
			"\tbne     0b        \n"
   	 : "=r"(upper),"=r"(lower),"=r"(tmp)
	  );
		return  (((uint64_t)upper) << 32) | lower;
}
#elif defined(__x86_64__)
static inline uint64_t getCycles()
{
    uint32_t tmp[2];
    __asm__ ("rdtsc" : "=a" (tmp[1]), "=d" (tmp[0]) : "c" (0x10) );
    return (((uint64_t)tmp[0]) << 32) | tmp[1];
}
#else
#error "unsupported architecture!"
#endif

#endif 

#include <utils.h>
#include <phase_profiling.h>

int
changeMode(uint64_t newMode) {
	
	bool success = false;
	modeIndicator_t indicator = { .value = 0 };
	modeIndicator_t expected = { .value = 0 };
	modeIndicator_t new = { .value = 0 };

	switch(newMode) {
		case SW:
			setProfilingReferenceTime();
			do {
				indicator = atomicReadModeIndicator();
				expected = setMode(indicator, HW);
				new = incDeferredCount(expected);
				success = boolCAS(&(modeIndicator.value), &(expected.value), new.value);
			} while (!success && (indicator.mode != SW));
			if (success) deferredTx = true;
			do {
				indicator = atomicReadModeIndicator();
				expected = setMode(indicator, HW);
				new = setMode(indicator, SW);
				success = boolCAS(&(modeIndicator.value), &(expected.value), new.value);
			} while (!success && (indicator.mode != SW));
			if(success){
				updateTransitionProfilingData(SW);
#if DESIGN == OPTIMIZED
				t0 = getCycles();
#endif 
			}
			break;
		case HW:
			setProfilingReferenceTime();
			do {
				indicator = atomicReadModeIndicator();
				expected = setMode(NULL_INDICATOR, SW);
				new = setMode(NULL_INDICATOR, HW);
				success = boolCAS(&(modeIndicator.value), &(expected.value), new.value);
			} while (!success && (indicator.mode != HW));
			if(success){
				updateTransitionProfilingData(HW);
			}
			break;
#if DESIGN == OPTIMIZED
		case GLOCK:
			do {
				indicator = atomicReadModeIndicator();
				if ( indicator.mode == SW ) return 0;
				if ( indicator.mode == GLOCK) return -1;
				expected = setMode(NULL_INDICATOR, HW);
				new = setMode(NULL_INDICATOR, GLOCK);
				success = boolCAS(&(modeIndicator.value), &(expected.value), new.value);
			} while (!success);
			updateTransitionProfilingData(GLOCK);
			break;
#endif 
		default:
			fprintf(stderr,"error: unknown mode %lu\n", newMode);
			exit(EXIT_FAILURE);
	}
	return 1;
}

#if DESIGN == OPTIMIZED
static inline
void
unlockMode(){
	bool success;
	do {
		modeIndicator_t expected = setMode(NULL_INDICATOR, GLOCK);
		modeIndicator_t new = setMode(NULL_INDICATOR, HW);
		success = boolCAS(&(modeIndicator.value), &(expected.value), new.value);
	} while (!success);
	updateTransitionProfilingData(HW);
}
#endif 

bool
HTM_Start_Tx() {

	phase_profiling_start();
	
	htm_retries = 0;
	abort_reason = 0;
#if DESIGN == OPTIMIZED
	t0 = getCycles();
#endif 

	while (true) {
		uint32_t status = htm_begin();
		if (htm_has_started(status)) {
			if (modeIndicator.value == 0) {
				return false;
			} else {
				htm_abort();
			}
		}

		
		if ( isModeSW() ){
      sum_cycles = 0;
      sum_tx_cycles = 0;
      sum_commits = 0;
			return true;
		}

#if DESIGN == OPTIMIZED
		if ( isModeGLOCK() ){
			while( isModeGLOCK() ) pthread_yield();
			continue;
		}
#endif 

		abort_reason = htm_abort_reason(status);
		__inc_abort_counter(abort_reason);
		
		modeIndicator_t indicator = atomicReadModeIndicator();
		if (indicator.value != 0) {
			return true;
		}

#if DESIGN == PROTOTYPE
		htm_retries++;
		if ( htm_retries >= HTM_MAX_RETRIES ) {
			changeMode(SW);
			return true;
		}
#else  
		htm_retries++;

		uint64_t t1 = getCycles();
		uint64_t abort_cycles = t1 - t0;
		t0 = t1;
		sum_cycles += abort_cycles;

		if (sum_cycles > MODE_CYCLES_THRESHOLD) {
      hw_throughput = sum_commits / (sum_cycles / 1000000000.0);
//printf("%ld %lf\n",pthread_self(),hw_throughput);
      uint64_t mean_cycles = 0;
      if (sum_commits) {
        mean_cycles = sum_tx_cycles / sum_commits;
      }

      sum_cycles = 0;
      sum_tx_cycles = 0;
      sum_commits = 0;

      if ( (hw_throughput < sw_throughput)
          || (mean_cycles > TX_CYCLES_THRESHOLD) ) {
        
			  changeMode(SW);
			  return true;
      }

		} else if (htm_retries >= HTM_MAX_RETRIES) {
			int status = changeMode(GLOCK);
			if(status == 0){
				
				return true;
			} else {
				
				if ( status == 1 ){
					htm_retries = 0;
					
					htm_global_lock_is_mine = true;
					t0 = getCycles();
					return false;
				} else {
					
					while( isModeGLOCK() ) pthread_yield();
					continue;
				}
			}
		}
#endif 
	}
}

void
HTM_Commit_Tx() {

#if	DESIGN == PROTOTYPE
	htm_end();
	__inc_commit_counter();
#else  
	if (htm_global_lock_is_mine){
		unlockMode();
		htm_global_lock_is_mine = false;
	} else {
		htm_end();
		__inc_commit_counter();
  }

  uint64_t t1 = getCycles();
  uint64_t tx_cycles = t1 - t0;
  t0 = t1;
  sum_cycles += tx_cycles;
  sum_tx_cycles += tx_cycles;

  sum_commits++;
#endif 

}


bool
STM_PreStart_Tx(bool restarted) {
	
	modeIndicator_t indicator = { .value = 0 };
	modeIndicator_t expected = { .value = 0 };
	modeIndicator_t new = { .value = 0 };
	bool success;

  if (unlikely(!has_already_transited_to_sw)) {
    
    sw_throughput = 0;
    has_already_transited_to_sw = true;
  }

	if (!deferredTx) {
		do {
			indicator = atomicReadModeIndicator();
				if (indicator.deferredCount == 0 || indicator.mode == HW) {
          if (decUndefCounter) atomicDecUndeferredCount();
          decUndefCounter = false;
          sum_cycles = 0;
          sum_tx_cycles = 0;
          sum_commits = 0;
					changeMode(HW);
					return true;
				}
      if(decUndefCounter) break;
			expected = setMode(indicator, SW);
			new = incUndeferredCount(expected);
			success = boolCAS(&(modeIndicator.value), &(expected.value), new.value);
		} while (!success);
    decUndefCounter = true;
	}
#if DESIGN == OPTIMIZED
	else {
    
    if (restarted) {
		  uint64_t t1 = getCycles();
		  uint64_t abort_cycles = t1 - t0;
		  t0 = t1;
		  sum_cycles += abort_cycles;

      if (sum_cycles > MODE_CYCLES_THRESHOLD) {
        sw_throughput = sum_commits / (sum_cycles / 1000000000.0);
//printf("%ld %lf\n",pthread_self(),sw_throughput);
        uint64_t mean_cycles = 0;
        if (sum_commits) {
          mean_cycles = sum_tx_cycles / sum_commits;
        }

        sum_cycles = 0;
        sum_tx_cycles = 0;
        sum_commits = 0;

        if ( (sw_throughput < hw_throughput)
              || (mean_cycles < TX_CYCLES_THRESHOLD) ) {
          deferredTx = false;
          atomicDecDeferredCount();
          modeIndicator_t new = atomicReadModeIndicator();
          if (new.deferredCount == 0) {
            changeMode(HW);
            return true;
          }
        }
      }
    } else {
		  t0 = getCycles();
    }
	}
#endif 
	return false;
}

void
STM_PostCommit_Tx() {

#if DESIGN == OPTIMIZED
	if (deferredTx) {
		uint64_t t1 = getCycles();
		uint64_t tx_cycles = t1 - t0;
		t0 = t1;
		sum_cycles += tx_cycles;
		sum_tx_cycles += tx_cycles;

    sum_commits++;

    if (sum_cycles < MODE_CYCLES_THRESHOLD) {
      return;
    }

    sw_throughput = sum_commits / (sum_cycles / 1000000000.0);
//printf("%ld %lf\n",pthread_self(),sw_throughput);
    uint64_t mean_cycles = sum_tx_cycles / sum_commits;

    if ( (sw_throughput > hw_throughput)
          && (mean_cycles > TX_CYCLES_THRESHOLD) ){
     sum_cycles = 0;
      sum_tx_cycles = 0;
      sum_commits = 0;
      return;
    }
    }
#endif 
	
  if (!deferredTx) {
    atomicDecUndeferredCount();
    decUndefCounter = false;
  } else { 
		deferredTx = false;
    atomicDecDeferredCount();
    modeIndicator_t new = atomicReadModeIndicator();
		if (new.deferredCount == 0) {
      sum_commits = 0;
      sum_cycles = 0;
      sum_tx_cycles = 0;
			changeMode(HW);
		}
	}
}

void
phTM_init(){
	printf("DESIGN: %s\n", (DESIGN == PROTOTYPE) ? "PROTOTYPE" : "OPTIMIZED");
	phase_profiling_init();
}

void
phTM_thread_init(){
	__init_prof_counters();
}

void
phTM_thread_exit(uint64_t stmCommits, uint64_t stmAborts){
	phase_profiling_stop();
#if DESIGN == OPTIMIZED
	if (deferredTx) {
		deferredTx = false;
		atomicDecDeferredCount();
	}
#endif 
	__term_prof_counters(stmCommits, stmAborts);
}

void
phTM_term() {
	__report_prof_counters();
	phase_profiling_report();
}

