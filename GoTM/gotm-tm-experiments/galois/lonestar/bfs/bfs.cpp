/*
 * This file belongs to the Galois project, a C++ library for exploiting parallelism.
 * The code is being released under the terms of the 3-Clause BSD License (a
 * copy is located in LICENSE.txt at the top-level directory).
 *
 * Copyright (C) 2018, The University of Texas at Austin. All rights reserved.
 * UNIVERSITY EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES CONCERNING THIS
 * SOFTWARE AND DOCUMENTATION, INCLUDING ANY WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR ANY PARTICULAR PURPOSE, NON-INFRINGEMENT AND WARRANTIES OF
 * PERFORMANCE, AND ANY WARRANTY THAT MIGHT OTHERWISE ARISE FROM COURSE OF
 * DEALING OR USAGE OF TRADE.  NO WARRANTY IS EITHER EXPRESS OR IMPLIED WITH
 * RESPECT TO THE USE OF THE SOFTWARE OR DOCUMENTATION. Under no circumstances
 * shall University be liable for incidental, special, indirect, direct or
 * consequential damages or loss of profits, interruption of business, or
 * related expenses which may arise from use of Software or Documentation,
 * including but not limited to those resulting from defects in Software and/or
 * Documentation, or loss or inaccuracy of data of any kind.
 */

// This examples shows
// 1. how to pass a range for data-driven algorithms
// 2. how to add new work items using context
// 3. how to specify schedulers
// 4. how to write an indexer for OBIM
#include "galois/Timer.h"
#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"

#include <iostream>
#include <string>
#include <chrono>

#include <stdlib.h>
#include <time.h>>

struct NodeD{
	int id;
	unsigned int value;
};

using Graph = galois::graphs::LC_Linear_Graph<NodeD, unsigned int>;
using GNode = Graph::GraphNode;
using UpdateRequest = std::pair<unsigned, GNode>;

static const unsigned int DIST_INFINITY = 14000000000;

int MAX_BLOCK_SIZE = 32;


constexpr unsigned int stepShift = 14;

int main(int argc, char** argv) {
  galois::SharedMemSys G;
  const int CHUNK_SIZE = 32;//atoi(argv[3]);
  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int NumThreads = atoi(argv[2]);
  std::cout << "NumThreads: " << NumThreads << std::endl;
  galois::setActiveThreads(NumThreads);// Galois will cap at hw max
  MAX_BLOCK_SIZE = 1;
  std::cout << "MAX_BLOCK_SIZE: " << MAX_BLOCK_SIZE << std::endl;
  int start_node = 0;
  if(argv[3] != NULL)
  	start_node = atoi(argv[3]);

  Graph graph;
  galois::graphs::readGraph(graph,
                            argv[1]); // argv[1] is the file name for graph

  // initialization
  
    srand (1234);
  galois::do_all(galois::iterate(graph),
                 [&graph](GNode N) {
                  auto& sdata =  graph.getData(N);
                  sdata = 1410065408;
                  for (auto ii : graph.edges(N))
                  {
                    auto& weight   = graph.getEdgeData(ii);
                    weight = rand() % 10 + 1;
                  }
                 }
  );

 
  galois::StatTimer T;
  T.start();

  //! [SSSP push operator]
  // SSSP operator
  // auto& ctx expands to galois::UserContext<GNode>& ctx
  auto SSSP = [&](GNode active_node, auto& ctx) {
    
    		//std::cout << "active node: " << active_node << std::endl;
       
                auto srcData = graph.getData(active_node).value;
		//std::cout << "srcData: " << srcData << std::endl;
                
                for (auto ii : graph.edges(active_node))
                { 
                
                        auto dst      = graph.getEdgeDst(ii);
			//std::cout << "     dst: " << dst << std::endl;
                        //auto weight   = graph.getEdgeData(ii);
			//std::cout << "     weight: " << weight << std::endl;
                        auto& dstData = graph.getData(dst).value;
                        if (dstData > 1 + srcData)
                        {
				//std::cout << "     pushing dst node... " << std::endl;
                                dstData = 1 + srcData;

                                ctx.push(dst);
                        }
                }
                
  };
  
/*  
  auto SSSP = [&](GNode active_node, auto& ctx) {
    // Get the value on the node
    auto srcData = graph.getData(active_node).value;

    // loop over neighbors to compute new value
    for (auto ii : graph.edges(active_node)) { // cautious point
      auto dst      = graph.getEdgeDst(ii);
      auto weight   = graph.getEdgeData(ii);
      auto& dstData = graph.getData(dst).value;
      if (dstData > weight + srcData) {
        dstData = weight + srcData;
        ctx.push(dst); // add new work items
      }
    }


  };
  */
  //! [SSSP push operator]

  //! [Scheduler examples]
  // Priority Function in SSSPPushSimple
  // Map user-defined priority to a bucket number in OBIM
  auto reqIndexer = [&](const GNode& N) {
    return (graph.getData(N, galois::MethodFlag::UNPROTECTED).value >> stepShift);
  };

  using namespace galois::worklists;
  using PSchunk = ChunkFIFO<CHUNK_SIZE>; // chunk size 16

  // clear source
int id = 0;
for (auto ii : graph)  
{
	if (id == start_node)
	{
  graph.getData(ii).value = 0;//graph.getData(*graph.begin()).value = 0;

    auto start = std::chrono::high_resolution_clock::now();
    galois::for_each(
        galois::iterate(
            {ii}), // initial range using initializer list
        SSSP                   // operator
        ,
        galois::wl<PSchunk>() // options. PSchunk expands to 
                              // galois::worklists::PerSocketChunkLIFO<16>, 
                              // where 16 is chunk size
        ,
	galois::loopname("sssp_dchunk16"));
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << "\n";

	}
	id++;
}

  T.stop();
  //printing node's path cost 
// int iter = 0;
// for (auto ii : graph)
// {
//   std::cout <<  graph.getData(ii).value << std::endl;
//   if (iter == 100)
//           break;
//   iter++;
// }
//iter=0;
// for (auto ii : graph)
// {
//	if (iter == 122)
//		std::cout << graph.getData(ii).id << " : " << graph.getData(ii).value << std::endl;
//	iter++;
// }
 
  return 0;
}
