//wcc//

#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"
#include <utility>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <fstream>

const unsigned int LABEL_INF = std::numeric_limits<unsigned int>::max();
using component_type = unsigned int;

struct LNode {    
    component_type comp_current;
  };

using Graph =  galois::graphs::LC_Linear_Graph<LNode, void>::with_no_lockable<false>::type::with_numa_alloc<true>::type;
using GNode = Graph::GraphNode;

const int CHUNK_SIZE = 32;
using PSchunk = galois::worklists::ChunkFIFO<CHUNK_SIZE>;

void LabelPropAlgo (Graph& graph) {
 
      galois::for_each(
          galois::iterate(graph),
          [&](const GNode& src, auto& ctx) {
            LNode& sdata = graph.getData(src);
              for(auto begin : graph.edges(src)){
                GNode dst              = graph.getEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (ddata.comp_current > sdata.comp_current)
                {
                        ddata.comp_current = sdata.comp_current;
                        ctx.push(dst);
                }
              }
          },
          galois::wl<PSchunk>(),     
          galois::loopname("LabelPropAlgo"));
}

int main(int argc, char** argv)
{
	galois::SharedMemSys G;
	Graph graph;
	  
	char* filename = argv[1];
	std::cout << "reading Graph " << filename << "\n";
	galois::graphs::readGraph(graph, filename);
	std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
	int numThreads = atoi(argv[2]);
	std::cout << "numThreads: " << numThreads << std::endl;
	galois::setActiveThreads(numThreads); 
	

  
  
	int id = 0;
	for (auto ii = graph.begin(), ei = graph.end(); ii != ei; ++ii) 
	{
		graph.getData(*ii).comp_current = id;
		id++;
	}
  std::cout <<"starting transpose label propagation..." << std::endl;
  auto start = std::chrono::high_resolution_clock::now();	
  LabelPropAlgo(graph);	
  auto finish = std::chrono::high_resolution_clock::now();


  std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << "\n";
	return 0;
}


//scc//

/*
#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"
#include <utility>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <fstream>

#include "galois/graphs/B_LC_CSR_Graph.h"
#include "galois/graphs/BufferedGraph.h"


const unsigned int LABEL_INF = std::numeric_limits<unsigned int>::max();
using component_type = unsigned int;

struct LNode {    
    component_type comp_current;

    component_type comp_current_t;

    bool taken;
  };

using Graphb    = galois::graphs::B_LC_CSR_Graph<LNode, void, false, false, true, true>;
using GNodeb = Graphb::GraphNode;


const int CHUNK_SIZE = 32;
using PSchunk = galois::worklists::ChunkFIFO<CHUNK_SIZE>;

void LabelPropAlgo (Graphb& graph, galois::InsertBag<GNodeb>& wl) {
 
      galois::for_each(
          galois::iterate(wl),
          [&](const GNodeb& src, auto& ctx) {
            LNode& sdata = graph.getData(src);
              for(auto begin : graph.edges(src)){
                GNodeb dst              = graph.getEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (!ddata.taken && (ddata.comp_current > sdata.comp_current))
                {
                        ddata.comp_current = sdata.comp_current;
                        ctx.push(dst);
                }
              }
          },    
          galois::wl<PSchunk>(),     
          galois::loopname("LabelPropAlgo"));
    
}

void LabelPropAlgoTransp (Graphb& graph, galois::InsertBag<GNodeb>& wl) {
 
      galois::for_each(
          galois::iterate(wl),
          [&](const GNodeb& src, auto& ctx) {
            LNode& sdata = graph.getData(src);
              for(auto begin : graph.in_edges(src)){
                GNodeb dst              = graph.getInEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (!ddata.taken && (ddata.comp_current_t > sdata.comp_current_t))
                {
                        ddata.comp_current_t = sdata.comp_current_t;
                        ctx.push(dst);
                }
              }
          },     
          galois::wl<PSchunk>(),     
          galois::loopname("LabelPropAlgo"));

}

int main(int argc, char** argv)
{
	galois::SharedMemSys G;
	  
	char* filename = argv[1];
	std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
	int numThreads = atoi(argv[2]);
	std::cout << "numThreads: " << numThreads << std::endl;
	galois::setActiveThreads(numThreads); 
	
  std::cout << "reading Graph " << filename << "\n";
  Graphb bcGraph;
  galois::graphs::BufferedGraph<void> fileReader;
  fileReader.loadGraph(filename);
  bcGraph.allocateFrom(fileReader.size(), fileReader.sizeEdges());
  bcGraph.constructNodes();
  galois::do_all(galois::iterate((uint32_t)0, fileReader.size()),
                 [&](uint32_t i) {
                   auto b = fileReader.edgeBegin(i);
                   auto e = fileReader.edgeEnd(i);

                   bcGraph.fixEndEdge(i, *e);

                   while (b < e) {
                     bcGraph.constructEdge(*b, fileReader.edgeDestination(*b));
                     b++;
                   }
                 });
  bcGraph.constructIncomingEdges();

   galois::InsertBag<GNodeb> wl;
	int id = 0;
	for (auto ii = bcGraph.begin(), ei = bcGraph.end(); ii != ei; ++ii) 
	{
		bcGraph.getData(*ii).comp_current = id;

        bcGraph.getData(*ii).comp_current_t = id;
        bcGraph.getData(*ii).taken = false;
		id++;

    wl.push(*ii);
	}
  std::cout <<"starting label propagation..." << std::endl;
  
  auto start = std::chrono::high_resolution_clock::now();	
  
  while(!wl.empty())
  {
    std::cout << "here\n";
    LabelPropAlgo(bcGraph, wl);
    LabelPropAlgoTransp (bcGraph, wl);
    id = 0;
    wl.clear();
    for (auto ii = bcGraph.begin(), ei = bcGraph.end(); ii != ei; ++ii)
    {
      auto& src = bcGraph.getData(*ii);
      if(src.comp_current == src.comp_current_t)
      {
        src.taken = true;
      }
      else
      {
        bcGraph.getData(*ii).comp_current = id;
        bcGraph.getData(*ii).comp_current_t = id;
        wl.push(*ii);
      }
      id++;

    }
  }
  auto finish = std::chrono::high_resolution_clock::now();

  id = 0;
  for(auto ii : bcGraph)
  {
    std::cout << id << " " << bcGraph.getData(ii).comp_current << std::endl;
    id++;
    if (id == 20)
    	break;
  }


  std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << "\n";
	return 0;
}*/

/*#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"
#include <utility>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <fstream>

#include "galois/graphs/B_LC_CSR_Graph.h"
#include "galois/graphs/BufferedGraph.h"


const unsigned int LABEL_INF = std::numeric_limits<unsigned int>::max();
using component_type = unsigned int;

struct LNode {    
    component_type comp_current;
//    component_type comp_old;

    component_type comp_current_t;
//    component_type comp_old_t;

    bool taken;
  };

////// using bidirectional graph                                     lock   numa
using Graphb    = galois::graphs::B_LC_CSR_Graph<LNode, void, false, false, true, true>;
using GNodeb = Graphb::GraphNode;
//////


const int CHUNK_SIZE = 32;
using PSchunk = galois::worklists::ChunkFIFO<CHUNK_SIZE>;

void LabelPropAlgo (Graphb& graph, galois::InsertBag<GNodeb>& wl) {
 
      galois::for_each(
          galois::iterate(wl),
          [&](const GNodeb& src, auto& ctx) {
            LNode& sdata = graph.getData(src);
            //std::cout <<  sdata.comp_current << " << sdata.comp_current" << std::endl;
//            if (sdata.comp_old > sdata.comp_current) {
              for(auto begin : graph.edges(src)){
                GNodeb dst              = graph.getEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (!ddata.taken && (ddata.comp_current > sdata.comp_current))
                {
                        ddata.comp_current = sdata.comp_current;
                        ctx.push(dst);
                }
              }
//              sdata.comp_old = sdata.comp_current;
//              ctx.push(src);
//            }
          },
          //galois::no_conflicts(),      
          galois::wl<PSchunk>(),     
          galois::loopname("LabelPropAlgo"));
          
    //unsigned int id = 0;
    //for(auto ii : graph)
    //{
    //  std::cout << id << " " << graph.getData(ii).comp_current << std::endl;
    //  id++;
    //}
}

void LabelPropAlgoTransp (Graphb& graph, galois::InsertBag<GNodeb>& wl) {
 
      galois::for_each(
          galois::iterate(wl),
          [&](const GNodeb& src, auto& ctx) {
            LNode& sdata = graph.getData(src);
            //std::cout << "srs old " << sdata.comp_old << " current " << sdata.comp_current << std::endl;
//            if (sdata.comp_old > sdata.comp_current) {
              for(auto begin : graph.in_edges(src)){
                GNodeb dst              = graph.getInEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (!ddata.taken && (ddata.comp_current_t > sdata.comp_current_t))
                {
                        ddata.comp_current_t = sdata.comp_current_t;
                        ctx.push(dst);
                }
              }
//              sdata.comp_old = sdata.comp_current;
//              ctx.push(src);
//            }
          },
          //galois::no_conflicts(),      
          galois::wl<PSchunk>(),     
          galois::loopname("LabelPropAlgo"));
          
    //unsigned int id = 0;
    //for(auto ii : graph)
    //{
    //  std::cout << id << " " << graph.getData(ii).comp_current_t << std::endl;
    //  id++;
    //}
}

int main(int argc, char** argv)
{
	galois::SharedMemSys G;
	//Graph graph;
	  
	char* filename = argv[1];
	//std::cout << "reading Graph " << filename << "\n";
	//galois::graphs::readGraph(graph, filename); // argv[1] is the file name for graph
	std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
	int numThreads = atoi(argv[2]);
	std::cout << "numThreads: " << numThreads << std::endl;
	galois::setActiveThreads(numThreads); // argv[2] is # of threads


  ///////// this reads the bidireccional graph
  std::cout << "reading Graph " << filename << "\n";
  Graphb bcGraph;
  galois::graphs::BufferedGraph<void> fileReader;
  fileReader.loadGraph(filename);
  bcGraph.allocateFrom(fileReader.size(), fileReader.sizeEdges());
  bcGraph.constructNodes();
  galois::do_all(galois::iterate((uint32_t)0, fileReader.size()),
                 [&](uint32_t i) {
                   auto b = fileReader.edgeBegin(i);
                   auto e = fileReader.edgeEnd(i);

                   bcGraph.fixEndEdge(i, *e);

                   while (b < e) {
                     bcGraph.constructEdge(*b, fileReader.edgeDestination(*b));
                     b++;
                   }
                 });
  bcGraph.constructIncomingEdges();

   galois::InsertBag<GNodeb> wl;
  // Initialize transpose vertices as individual components
	int id = 0;
	for (auto ii = bcGraph.begin(), ei = bcGraph.end(); ii != ei; ++ii) 
	{
		bcGraph.getData(*ii).comp_current = id;
		//bcGraph.getData(*ii).comp_old     = LABEL_INF;

        bcGraph.getData(*ii).comp_current_t = id;
		//bcGraph.getData(*ii).comp_old_t     = LABEL_INF;
        bcGraph.getData(*ii).taken = false;
		id++;
    //insert vertex in worklist
    wl.push(*ii);
	}
  std::cout <<"starting label propagation..." << std::endl;
  
  auto start = std::chrono::high_resolution_clock::now();	
  
  while(!wl.empty())
  {
    //std::cout << "here\n";
    LabelPropAlgo(bcGraph, wl);
    LabelPropAlgoTransp (bcGraph, wl);
    id = 0;
    wl.clear();
    for (auto ii = bcGraph.begin(), ei = bcGraph.end(); ii != ei; ++ii)
    {
      auto& src = bcGraph.getData(*ii);
      if(src.comp_current == src.comp_current_t)
      {
        src.taken = true;
      }
      else
      {
        bcGraph.getData(*ii).comp_current = id;
        //bcGraph.getData(*ii).comp_old     = LABEL_INF;
        bcGraph.getData(*ii).comp_current_t = id;
        //bcGraph.getData(*ii).comp_old_t     = LABEL_INF;
        wl.push(*ii);
      }
      id++;

    }
  }
  auto finish = std::chrono::high_resolution_clock::now();
  /////////
  id = 0;
  for(auto ii : bcGraph)
  {
    std::cout << id << " " << bcGraph.getData(ii).comp_current << std::endl;
    id++;
  }


  std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << "\n";
	return 0;
}*/
