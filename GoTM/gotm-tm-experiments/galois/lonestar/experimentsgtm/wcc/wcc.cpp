#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"
#include <utility>
#include <vector>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <fstream>

const unsigned int LABEL_INF = std::numeric_limits<unsigned int>::max();
using component_type = unsigned int;

struct LNode {    
    component_type comp_current;
  };

using Graph =  galois::graphs::LC_Linear_Graph<LNode, void>::with_no_lockable<false>::type::with_numa_alloc<true>::type;
using GNode = Graph::GraphNode;

const int CHUNK_SIZE = 32;
using PSchunk = galois::worklists::ChunkFIFO<CHUNK_SIZE>;

void LabelPropAlgo (Graph& graph) {
 
      galois::for_each(
          galois::iterate(graph),
          [&](const GNode& src, auto& ctx) {
            LNode& sdata = graph.getData(src);
              for(auto begin : graph.edges(src)){
                GNode dst              = graph.getEdgeDst(begin);
                auto& ddata            = graph.getData(dst);
                if (ddata.comp_current > sdata.comp_current)
                {
                        ddata.comp_current = sdata.comp_current;
                        ctx.push(dst);
                }
              }
          },
          galois::wl<PSchunk>(),     
          galois::loopname("LabelPropAlgo"));
}

int main(int argc, char** argv)
{
	galois::SharedMemSys G;
	Graph graph;
	  
	char* filename = argv[1];
	std::cout << "reading Graph " << filename << "\n";
	galois::graphs::readGraph(graph, filename);
	std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
	int numThreads = atoi(argv[2]);
	std::cout << "numThreads: " << numThreads << std::endl;
	galois::setActiveThreads(numThreads); 
	

  
  
	int id = 0;
	for (auto ii = graph.begin(), ei = graph.end(); ii != ei; ++ii) 
	{
		graph.getData(*ii).comp_current = id;
		id++;
	}
  std::cout <<"starting transpose label propagation..." << std::endl;
  auto start = std::chrono::high_resolution_clock::now();	
  LabelPropAlgo(graph);	
  auto finish = std::chrono::high_resolution_clock::now();


  std::chrono::duration<double> elapsed = finish - start;
	std::cout << "Elapsed time: " << elapsed.count() << "\n";
	return 0;
}

