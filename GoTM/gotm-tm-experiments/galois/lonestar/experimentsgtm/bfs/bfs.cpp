
#include "galois/Timer.h"
#include "galois/Galois.h"
#include "galois/graphs/LCGraph.h"

#include <iostream>
#include <string>
#include <chrono>

#include <stdlib.h>
#include <time.h>>

struct NodeD{
	int id;
	unsigned int value;
};

using Graph = galois::graphs::LC_Linear_Graph<NodeD, unsigned int>;
using GNode = Graph::GraphNode;
using UpdateRequest = std::pair<unsigned, GNode>;

static const unsigned int DIST_INFINITY = 14000000000;



constexpr unsigned int stepShift = 14;

int main(int argc, char** argv) {
  galois::SharedMemSys G;
  const int CHUNK_SIZE = 32;
  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int NumThreads = atoi(argv[2]);
  std::cout << "NumThreads: " << NumThreads << std::endl;
  galois::setActiveThreads(NumThreads);
  int start_node = 0;
  if(argv[3] != NULL)
  	start_node = atoi(argv[3]);

  Graph graph;
  galois::graphs::readGraph(graph,argv[1]); 
  
  srand (1234);
  galois::do_all(galois::iterate(graph),
                 [&graph](GNode N) {
                  auto& sdata =  graph.getData(N);
                  sdata = 1410065408;
                  for (auto ii : graph.edges(N))
                  {
                    auto& weight   = graph.getEdgeData(ii);
                    weight = rand() % 10 + 1;
                  }
                 }
  );

 
  galois::StatTimer T;
  T.start();

  auto BFS = [&](GNode active_node, auto& ctx) {
    
       
                auto srcData = graph.getData(active_node).value;
                
                for (auto ii : graph.edges(active_node))
                { 
                        auto dst      = graph.getEdgeDst(ii);
                        auto& dstData = graph.getData(dst).value;
                        if (dstData > 1 + srcData)
                        {
                                dstData = 1 + srcData;
                                ctx.push(dst);
                        }
                }
                
  };
  

  using namespace galois::worklists;
  using PSchunk = ChunkFIFO<CHUNK_SIZE>; 

  
int id = 0;
for (auto ii : graph)  
{
	if (id == start_node)
	{
  graph.getData(ii).value = 0;

    auto start = std::chrono::high_resolution_clock::now();
    galois::for_each(
        galois::iterate(
            {ii}),BFS,galois::wl<PSchunk>());
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << "\n";

	}
	id++;
}

  T.stop();
 
  return 0;
}
