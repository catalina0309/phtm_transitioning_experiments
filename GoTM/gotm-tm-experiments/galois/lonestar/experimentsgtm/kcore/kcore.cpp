  
#include "galois/Galois.h"
#include "galois/gstl.h"
#include "galois/Reduction.h"
#include "galois/AtomicHelpers.h"
#include "galois/graphs/LCGraph.h"

 const int CHUNK_SIZE = 32;


struct NodeData {

  uint32_t currentDegree;

};


using Graph = galois::graphs::LC_Linear_Graph<NodeData, void>::with_no_lockable<false>::type;

using GNode = Graph::GraphNode;


void degreeCounting(Graph& graph) {
        for (auto node : graph )
        {
                auto& nodeData = graph.getData(node);
                nodeData.currentDegree = 0;
        }
        for (auto node : graph )
        {
                for( auto e : graph.edges(node))
                {
                        GNode dst = graph.getEdgeDst(e);
                        auto& dstData = graph.getData(dst);
                        dstData.currentDegree ++;
                }
        }
};



void setupInitialWorklist(Graph& graph,
                          galois::InsertBag<GNode>& initialWorklist,  unsigned int k_core_num ) {
  galois::do_all(
    galois::iterate(graph.begin(), graph.end()),
    [&] (GNode curNode) {
      NodeData& curData = graph.getData(curNode);
      if (curData.currentDegree < k_core_num) {
        // dead node, add to initialWorklist for processing later
        initialWorklist.emplace(curNode);
      }
    },
    galois::loopname("InitialWorklistSetup"),
    galois::no_stats()
  );
}

void asyncCascadeKCore(Graph& graph, galois::InsertBag<GNode>& initialWorklist, unsigned int k_core_num) {
  galois::for_each(
    galois::iterate(initialWorklist),
    [&] (GNode deadNode, auto& ctx) {
      // decrement degree of all neighbors
      for (auto e : graph.edges(deadNode)) {
        GNode dest = graph.getEdgeDst(e);
        NodeData& destData = graph.getData(dest);

        destData.currentDegree = destData.currentDegree - 1u;
        if (destData.currentDegree == k_core_num)
        {
          ctx.push(dest);
        }

      }

    },
    galois::wl<galois::worklists::ChunkFIFO<CHUNK_SIZE>>(),
    galois::loopname("AsyncCascadeDeadNodes")
  );
}


int main(int argc, char** argv) {
  galois::SharedMemSys G;

  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int NumThreads = atoi(argv[2]);
  std::cout << "NumThreads: " << NumThreads << std::endl;
  galois::setActiveThreads(NumThreads);

  Graph graph;
  galois::graphs::readGraph(graph, argv[1]);
  
  // some initial stat reporting
   unsigned int k_core_num = graph.sizeEdges() / graph.size();
   std::cout << "k_core_num: " << k_core_num << std::endl;

  degreeCounting(graph);

  
  galois::StatTimer runtimeTimer;

  runtimeTimer.start();
  auto start = std::chrono::high_resolution_clock::now();

    galois::InsertBag<GNode> initialWorklist;
    setupInitialWorklist(graph, initialWorklist, k_core_num);
    
    std::cout << "performing comp...\n";
    asyncCascadeKCore(graph, initialWorklist, k_core_num);
  auto finish = std::chrono::high_resolution_clock::now();
  runtimeTimer.stop();

  std::chrono::duration<double> elapsed = finish - start;
  std::cout << "Elapsed time: " << elapsed.count() << "\n";

  return 0;
}


