
#include <chrono>

#include "Lonestar/BoilerPlate.h"
#include "PageRank-constants.h"
#include "galois/Bag.h"
#include "galois/Galois.h"
#include "galois/Timer.h"
#include "galois/graphs/LCGraph.h"
#include "galois/graphs/TypeTraits.h"

// These implementations are based on the Push-based PageRank computation
// (Algorithm 4) as described in the PageRank Europar 2015 paper.

const char* desc =
    "Computes page ranks a la Page and Brin. This is a push-style algorithm.";

constexpr static const unsigned CHUNK_SIZE = 32;

enum Algo { Async, Sync }; // Async has better asbolute performance.

static cll::opt<Algo> algo("algo", cll::desc("Choose an algorithm:"),
                           cll::values(clEnumVal(Async, "Async"),
                                       clEnumVal(Sync, "Sync"), clEnumValEnd),
                           cll::init(Async));

struct LNode {
  PRTy value;
  PRTy residual;

  void init() {
    value    = 0.0;
    residual = INIT_RESIDUAL;
  }

  friend std::ostream& operator<<(std::ostream& os, const LNode& n) {
    os << "{PR " << n.value << ", residual " << n.residual << "}";
    return os;
  }
};

typedef galois::graphs::LC_Linear_Graph<LNode, void> ::with_no_lockable<false>::type Graph;
typedef typename Graph::GraphNode GNode;

void asyncPageRank(Graph& graph) {
  typedef galois::worklists::ChunkFIFO<CHUNK_SIZE> WL;
  galois::for_each(
      galois::iterate(graph),
      [&](GNode src, auto& ctx) {
        LNode& sdata = graph.getData(src);
        

        if (sdata.residual > tolerance) {
          PRTy oldResidual = sdata.residual;
          PRTy newResidual = 0;
          PRTy value = sdata.value + oldResidual;
          int src_nout = std::distance(graph.edge_begin(src), graph.edge_end(src));
          if (src_nout > 0) {
            PRTy delta = oldResidual * ALPHA / src_nout;
            
            for (auto jj : graph.edges(src)) {
              GNode dst    = graph.getEdgeDst(jj);
              LNode& ddata = graph.getData(dst);
              if (delta > 0) {
                auto old = ddata.residual;
                ddata.residual += delta;
                if ((old < tolerance) && (old + delta >= tolerance)) {
                  ctx.push(dst);
                }
              }
            }
          }
          sdata.residual = newResidual;
          sdata.value = value;
        }
      },
      galois::loopname("PushResidualAsync"), 
      galois::wl<WL>());
}


int main(int argc, char** argv) {
  galois::SharedMemSys G;
  
  static const int numThreads = atoi(argv[2]);
  galois::setActiveThreads(numThreads); // argv[2] is # of threads
  galois::StatTimer T("OverheadTime");
  T.start();

  Graph graph;
  galois::graphs::readGraph(graph, argv[1]);
  std::cout << "Read " << graph.size() << " nodes, " << graph.sizeEdges()
            << " edges\n";

  galois::preAlloc(5 * numThreads +
                   (5 * graph.size() * sizeof(typename Graph::node_data_type)) /
                       galois::runtime::pagePoolSize());
  galois::reportPageAlloc("MeminfoPre");

  std::cout << "tolerance:" << tolerance << ", maxIterations:" << maxIterations
            << "\n";

  galois::do_all(galois::iterate(graph),
                 [&graph](GNode n) { graph.getData(n).init(); },
                 galois::no_stats(), galois::loopname("Initialize"));

  galois::StatTimer Tmain;
  Tmain.start();


    std::cout << "Running Edge Async push version" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    asyncPageRank(graph);
    auto finish = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << "\n";

  Tmain.stop();

  galois::reportPageAlloc("MeminfoPost");

  if (!skipVerify) {
    printTop(graph);
  }

#if DEBUG
  printPageRank(graph);
#endif

  T.stop();

  return 0;
}

