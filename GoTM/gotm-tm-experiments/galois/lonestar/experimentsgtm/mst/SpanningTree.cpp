#include "galois/Reduction.h"
#include "galois/Bag.h"
#include "galois/Timer.h"

#include "galois/graphs/LCGraph.h"
#include "galois/ParallelSTL.h"

#include <chrono>
#include <utility>
#include <algorithm>
#include <iostream>

typedef std::pair<int, int> Edge;
class Node {
  public:

  Node* m_component;
  int label;
  int touched;
  int mst[50];
  int edgeCount;

  Node () {
          m_component = this;
        edgeCount = 0;}

  bool isRep() const {
    return m_component == this;
  }

  Node* find() const {
    if (isRep())
      return m_component;

    Node* rep = m_component;
    while (rep->m_component != rep) {
      Node* next = rep->m_component;
      rep     = next;
    }
    return rep;
  }

  Node* component() { return find(); }
  void setComponent(Node* n) { m_component = n; }
};


typedef galois::graphs::LC_Linear_Graph<Node, void>::with_no_lockable<false>::type Graph;


typedef Graph::GraphNode GNode;

int main(int argc, char** argv)
{
        galois::SharedMemSys G;
        Graph graph;
	int start_node = 0;
        galois::graphs::readGraph(graph, argv[1]);
        std::cout << "Num nodes: " << graph.size() << std::endl;
        const int CHUNK_SIZE = 32;
        std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
        int NumThreads = atoi(argv[2]);
        std::cout << "NumThreads: " << NumThreads << std::endl;
        galois::setActiveThreads(NumThreads);
	if (argv[3] != NULL)
		start_node = atoi(argv[3]);
        int i = 0;
        for (auto ii : graph)
        {
                graph.getData(ii).label = i;
                graph.getData(ii).touched = 0;
                i++;
        }

        galois::StatTimer T;
        T.start();
        auto start = std::chrono::high_resolution_clock::now();  
        Graph::iterator ii = graph.begin(), ei = graph.end();
        if (ii != ei) {
                Node* root = &graph.getData(*ii);


                auto MST = [&](GNode src, auto& ctx) {

                        Node srcdata   = graph.getData(src);
                        auto begin = src->edgeBegin();
                        auto end = src->edgeEnd();
                        while (begin != end)
                        {
                                GNode dst   = graph.getEdgeDst(begin);
                                Node& ddata = graph.getData(dst);
                                if (!ddata.touched)
                                {
                                        ddata.touched = 1;
                                        srcdata.mst[srcdata.edgeCount] = ddata.label;
                                        srcdata.edgeCount++;
                                        ctx.push(dst);
                                }
                                *begin++;
                        }

                };
		int ind = 0;
		for (auto n : graph)
		{
			if ( start_node == ind)
			{


                		galois::for_each(galois::iterate({n}), MST, galois::loopname("MSTAlgo"), galois::wl<galois::worklists::ChunkFIFO<CHUNK_SIZE>>());

				break;
			}
			ind++;
		}
        }
        auto finish = std::chrono::high_resolution_clock::now();
        T.stop();

        std::chrono::duration<double> elapsed = finish - start;
        std::cout << "Elapsed time: " << elapsed.count() << "\n";
        return 0;
}

