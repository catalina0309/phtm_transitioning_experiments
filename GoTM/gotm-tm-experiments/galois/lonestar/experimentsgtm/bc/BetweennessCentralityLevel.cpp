constexpr static const char* const REGION_NAME = "BC";

#include <limits>
#include <fstream>
#include "galois/gstl.h"
#include "galois/Reduction.h"
#include "galois/Timer.h"
#include "galois/AtomicHelpers.h"
#include "galois/graphs/LCGraph.h"
#include <chrono>


using ShortPathType = double;


const uint32_t infinity        = std::numeric_limits<uint32_t>::max() / 4;
static uint64_t currentSrcNode = 0;

struct NodeData {
  uint32_t currentDistance;
  ShortPathType numShortestPaths;
  float dependency;
  float bc;
};


std::ifstream sourceFile;
std::vector<uint64_t> sourceVector;

using Graph = galois::graphs::LC_Linear_Graph<NodeData, void>::with_no_lockable<false>::type::with_numa_alloc<true>::type;
using GNode = Graph::GraphNode;
using WorklistType = galois::InsertBag<GNode, 4096>;

constexpr static const unsigned CHUNK_SIZE = 256u;


void InitializeGraph(Graph& graph) {
  galois::do_all(
    galois::iterate(graph),
    [&] (GNode n) {
      NodeData& nodeData = graph.getData(n);
      nodeData.currentDistance    = 0;
      nodeData.numShortestPaths   = 0;
      nodeData.dependency         = 0;
      nodeData.bc                 = 0;
    },
    
    galois::loopname("InitializeGraph")
  );
}


void InitializeIteration(Graph& graph, GNode currentSrcN) {
  galois::do_all(
    galois::iterate(graph),
    [&] (GNode n) {
      NodeData& nodeData = graph.getData(n);
      bool isSource = (n == currentSrcN);
      
      if (!isSource) {
        nodeData.currentDistance    = infinity;
        nodeData.numShortestPaths   = 0;
      } else {
        nodeData.currentDistance    = 0;
        nodeData.numShortestPaths   = 1;
      }
      
      nodeData.dependency         = 0;
    },
    
    galois::loopname("InitializeIteration")
  );
};

galois::gstl::Vector<WorklistType> SSSP(Graph& graph, GNode currentSrcN) {
  galois::gstl::Vector<WorklistType> stackOfWorklists;
  uint32_t currentLevel = 0;

  
  stackOfWorklists.emplace_back();
  stackOfWorklists[0].emplace(currentSrcN);

  
  while (!stackOfWorklists[currentLevel].empty()) {
    
    stackOfWorklists.emplace_back();
    uint32_t nextLevel = currentLevel + 1;

    galois::for_each(
      galois::iterate(stackOfWorklists[currentLevel]),
      [&] (GNode n,  auto& ctx) {

        NodeData& curData = graph.getData(n);
        for (auto e : graph.edges(n)) {
          GNode dest = graph.getEdgeDst(e);
          NodeData& destData = graph.getData(dest);    
                    if (destData.currentDistance == infinity) {
            uint32_t oldVal;
            
            if (destData.currentDistance == infinity) {
              oldVal =  destData.currentDistance;
              destData.currentDistance = nextLevel;
            }
            
            if (oldVal == infinity) {
              stackOfWorklists[nextLevel].emplace(dest);
            }

            destData.numShortestPaths = destData.numShortestPaths + curData.numShortestPaths;
            
          } else if (destData.currentDistance == nextLevel) {
            destData.numShortestPaths = destData.numShortestPaths + curData.numShortestPaths;
            
          }
          
        }

      },
      galois::no_pushes(),
      galois::chunk_size<CHUNK_SIZE>(),
      galois::loopname("SSSP")
    );

    
    currentLevel++;
  }
  return stackOfWorklists;
}

void BackwardBrandes(Graph& graph,
                     galois::gstl::Vector<WorklistType>& stackOfWorklists) {
  
  if (stackOfWorklists.size() >= 3) {
    uint32_t currentLevel = stackOfWorklists.size() - 3;

    
    while (currentLevel > 0) {
      WorklistType& currentWorklist = stackOfWorklists[currentLevel];
      uint32_t succLevel = currentLevel + 1;

      galois::for_each(
        galois::iterate(currentWorklist),
              [&] (GNode n, auto& ctx) {
          NodeData& curData = graph.getData(n);
          GALOIS_ASSERT(curData.currentDistance == currentLevel);

          for (auto e : graph.edges(n)) {
            GNode dest = graph.getEdgeDst(e);
            NodeData& destData = graph.getData(dest);

            if (destData.currentDistance == succLevel) {
              
              float contrib = ((float)1 + destData.dependency) /
                              destData.numShortestPaths;
              curData.dependency = curData.dependency + contrib;
            }
          }

         
          curData.dependency *= curData.numShortestPaths;
          
          curData.bc += curData.dependency;
        },
        galois::no_conflicts(),
        galois::no_pushes(),
        galois::chunk_size<CHUNK_SIZE>(),
        galois::loopname("Brandes")
      );

      
      currentLevel--;
    }
  }
}


void Sanity(Graph& graph) {
  galois::GReduceMax<float> accumMax;
  galois::GReduceMin<float> accumMin;
  galois::GAccumulator<float> accumSum;
  accumMax.reset();
  accumMin.reset();
  accumSum.reset();

  
  galois::do_all(
    galois::iterate(graph),
    [&] (GNode n) {
      NodeData& nodeData = graph.getData(n);
      accumMax.update(nodeData.bc);
           accumSum += nodeData.bc;
    },
    
    galois::loopname("Sanity")
  );

  galois::gPrint("Max BC is ", accumMax.reduce(), "\n");
  galois::gPrint("Min BC is ", accumMin.reduce(), "\n");
  galois::gPrint("BC sum is ", accumSum.reduce(), "\n");
}



int main(int argc, char** argv) {
  galois::SharedMemSys G;
  char* filename = argv[1];
  std::cout << "reading Graph " << filename << "\n";
  
  std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
  int numThreads = atoi(argv[2]);
  std::cout << "numThreads: " << numThreads << std::endl;
  galois::setActiveThreads(numThreads); 
  uint32_t numberOfSources = 1;

  
  galois::StatTimer totalTimer("TimerTotal", REGION_NAME);
  totalTimer.start();

  
  galois::StatTimer graphConstructTimer("TimerConstructGraph", "BFS");
  graphConstructTimer.start();
  Graph graph;
  galois::graphs::readGraph(graph, filename);
  graphConstructTimer.stop();
  galois::gInfo("Graph construction complete");

  
  uint64_t loop_end = numberOfSources;
  
  InitializeGraph(graph);

  galois::gInfo("Beginning main computation");
  galois::StatTimer runtimeTimer;
  auto start = std::chrono::high_resolution_clock::now();
  
  for (uint64_t i = 0; i < loop_end; i++) {
    
    runtimeTimer.start();

    InitializeIteration(graph,*graph.begin());
    
    galois::gstl::Vector<WorklistType> worklists = SSSP(graph, *graph.begin());
    BackwardBrandes(graph, worklists);

    runtimeTimer.stop();
  }
 totalTimer.stop();
  auto finish = std::chrono::high_resolution_clock::now();

  std::chrono::duration<double> elapsed = finish - start;
  std::cout << "Elapsed time: " << elapsed.count() << "\n";
  
  Sanity(graph);


  return 0;
}

