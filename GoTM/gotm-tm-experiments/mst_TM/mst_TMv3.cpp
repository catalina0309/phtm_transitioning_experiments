#include "galois/Galois.h"
#include "galois/Reduction.h"
#include "galois/Bag.h"
#include "galois/Timer.h"
#include "galois/graphs/LCGraph.h"
#include "galois/ParallelSTL.h"

#include <chrono>
#include <utility>
#include <algorithm>
#include <iostream>

struct Pair_edges{
        int v1;
        int v2;
};

struct Node {
  int label;
  int touched;
};

#ifdef TM
typedef galois::graphs::LC_Linear_Graph<Node, int>::with_no_lockable<true>::type  Graph;
#else
typedef galois::graphs::LC_Linear_Graph<Node, int>::with_no_lockable<true>::type Graph;
#endif

typedef Graph::GraphNode GNode;

int main(int argc, char** argv)
{
        galois::SharedMemSys G;
        Graph graph;
        int start_node = 0;
        galois::graphs::readGraph(graph, argv[1]);
        std::cout << "Num nodes: " << graph.size() << std::endl;
        const int CHUNK_SIZE = 32;
        std::cout << "CHUNK_SIZE: " << CHUNK_SIZE << std::endl;
        int NumThreads = atoi(argv[2]);
        std::cout << "NumThreads: " << NumThreads << std::endl;
        galois::setActiveThreads(NumThreads);
        if (argv[3] != NULL)
                start_node = atoi(argv[3]);
        int i = 0;
        long int ed=0;
        for (auto ii : graph)
        {
                graph.getData(ii).label = i;
                graph.getData(ii).touched = 0;
				for (auto e : graph.edges(ii))
				{
					graph.getEdgeData(e) = ed;					
					ed ++;
				}
                i++;
        }

        galois::StatTimer T;
        T.start();
        auto start = std::chrono::high_resolution_clock::now();
        galois::InsertBag<int> mst;
        Graph::iterator ii = graph.begin(), ei = graph.end();

        if (ii != ei) {
                Node* root = &graph.getData(*ii);


                auto MST = [&](GNode src, auto& ctx) {
#ifdef TM
                __transaction_atomic{
#endif
                 //       Node& srcdata   = graph.getData(src);
                        auto begin = src->edgeBegin();
                        auto end = src->edgeEnd();
                        while (begin != end)
                        {
                                GNode dst   = graph.getEdgeDst(begin);
                                Node& ddata = graph.getData(dst);
                                if (!ddata.touched)
                                {
                                        ddata.touched = 1;
                                        mst.emplace(graph.getEdgeData(begin));
                                        ctx.push(dst);
                                }
                                begin++;
                        }
#ifdef TM
                }
#endif
                };
                int ind = 0;
                for (auto n : graph)
                {
                        if ( start_node == ind)
                        {
                                Node& ddata = graph.getData(n);
                                ddata.touched = 1;
#ifdef TM
                                galois::for_each(galois::iterate({n}), MST, galois::loopname("MSTAlgo"), galois::no_conflicts(), galois::wl<galois::worklists::ChunkFIFO<CHUNK_SIZE>>());
#else
                                galois::for_each(galois::iterate({n}), MST, galois::loopname("MSTAlgo"), galois::wl<galois::worklists::ChunkFIFO<CHUNK_SIZE>>());
#endif
                                break;
                        }
                        ind++;
                }
        }
        auto finish = std::chrono::high_resolution_clock::now();
        T.stop();

        std::chrono::duration<double> elapsed = finish - start;
        std::cout << "Elapsed time: " << elapsed.count() << "\n";
        int ind = 0;
        for(auto ii = mst.begin(), ie = mst.end(); ii != ie; ++ii)
        {
                std::cout <<  *ii  << std::endl;
                ind++;
                if (ind == 20)
                        break;
        }

        return 0;
}
