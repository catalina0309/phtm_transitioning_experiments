phtm transitioning experiments
==============================

This repository contains all the libraries, and scripts required to test three versions of Phased TM implementations with different transitioning mechanisms. (1) Graph-oriented TM (GoTM), (2) Commit-throughput TM (CTTM), and (3) Phased TM* (PhTMs).

The available applications to test all phased TM mechanisms are:

Graph applications:
===================
- sssp_TM 
- mst_TM 
- bfs_TM 
- bc_TM 
- wcc_TM 
- pagerank_TM

Stamp benchmark:
================
- genome 
- labyrinth 
- ssca2 
- kmeans 
- intruder 
- vacation

There are two graph workloads (rmat-n30m-m120m soc-LiveJournal1) available in a tar.gz file at GoTM/gotm-tm-experiments/inputs. You can extract them inside the inputs folder to test the graph applications.

- A dockerfile is available in phtm_transitioning_experiments/ to install and run both graph and stamp applications. After building the docker, run the following command. 

1. docker run --rm -it --cap-add=SYS_PTRACE --security-opt seccomp=unconfined -v <path_to_phtm_transitioning_experiments_folder>:/app galois bash

- The graph applications are built using the Galois C++ library, so you need to install galois first. To compile and install Galois library in the Galois_lib/ folder placed inside the GoTM/gotm-tm-experiments folder run the following commands:

1. cd phtm_transitioning_experiments/GoTM/gotm-tm-experiments/galois
2. mkdir build
3. cd build
4. cmake -DCMAKE_INSTALL_PREFIX=/app/phtm_transitioning_experiments/GoTM/gotm-tm-experiments/Galois_lib -DSKIP_COMPILE_APPS=1 ..
5. make install
6. ccmake .

- To compile the graph applications with any phased TM runtime you must first compile the stamp benchmark inside the specific runtime. All the runtimes use Norec for SW mode. To compile the phased TM runtimes and the stamp benchmark with each runtime run the following commands:

for CTM: 
from the phtm_transitioning_experiments/ folder
1. cd CTM/ctm-experiments
2. ./scripts/compile -b gcctm -B phasedTM -S norec -s 'genome labyrinth ssca2 kmeans intruder vacation' -PHTM_STATUS_PROFILING -PTIME_MODE_PROFILING

for GoTM: 
from the phtm_transitioning_experiments/ folder
1. cd GoTM/gotm-tm-experiments/tm
2. ./scripts/compile -b gcctm -B phasedTM -S norec -s 'genome labyrinth ssca2 kmeans intruder vacation' -PHTM_STATUS_PROFILING -PTIME_MODE_PROFILING

for PhTMs:
from the phtm_transitioning_experiments/ folder
1. cd PhTMs/phtms-experiments
2. ./scripts/compile -b gcctm -B phasedTM -S norec -s 'genome labyrinth ssca2 kmeans intruder vacation' -PHTM_STATUS_PROFILING -PTIME_MODE_PROFILING

- Now you can compile the graph applications with the phased TM runtimes. To compile the graph applications with a particular runtime you can used the 'compile' script found inside the GoTM/gotm-tm-experiments/ folder:

you must set the following flags to run the compile script:
-s refers to the applications that will be compiled with.
-p the phased tm runtines each application will be compiled with.

from the phtm_transitioning_experiments/ folder:

1. cd GoTM/gotm-tm-experiments/
2. ./compile -s  'sssp_TM mst_TM bfs_TM bc_TM wcc_TM pagerank_TM' -p cttm
3. ./compile -s  'sssp_TM mst_TM bfs_TM bc_TM wcc_TM pagerank_TM' -p gotm
4. ./compile -s  'sssp_TM mst_TM bfs_TM bc_TM wcc_TM pagerank_TM' -p phtms

- You can now execute the graph applications. To execute the graph applications with PhasedTM:
set the following flags: 
-s refers to the applications that will be executed.
-t the number of threads it will be executed with.
-p the phased tm runtines each application will be executed with.
-i the workloads (these should be plased in the GoTM/gotm-tm-experiments/inputs folder, and must have the .gr extension.
-r the number of executions.

1. cd GoTM/gotm-tm-experiments/
2. ./execute  -s 'sssp_TM mst_TM bfs_TM bc_TM wcc_TM pagerank_TM' -t '1 2 4 8 12 18' -p 'phtms gotm cttm' -i ' rmat-n30m-m120m soc-LiveJournal1' -r 10 -q 0

- To execute the stamp benchmark with each runtime from the phtm_transitioning_experiments/ folder:

CTM: 
1. cd CTM/ctm-experiments
2. ./scripts/execute -b gcctm -B phasedTM -S norec -s 'genome labyrinth ssca2 kmeans intruder vacation' -t '1 2 4 8 12 18' -n 10

GoTM: 
1. cd GoTM/gotm-tm-experiments/tm
2. ./scripts/execute -b gcctm -B phasedTM -S norec -s 'genome labyrinth ssca2 kmeans intruder vacation' -t '1 2 4 8 12 18' -n 10

PhTMs:
1. cd PhTMs/phtms-experiments
2. ./scripts/execute -b gcctm -B phasedTM -S norec -s 'genome labyrinth ssca2 kmeans intruder vacation' -t '1 2 4 8 12 18' -n 10



- To plot the normalized speedup of each application (graphs and stamp), you must first compile and execute the sequential baseline. To compile the stamp benchmark for sequential execution from the phtm_transitioning_experiments/ folder:

1. cd PhTMs/phtms-experiments
2. ./scripts/compile -b seq -s 'genome labyrinth ssca2 kmeans intruder vacation' 

- To execute the stamp bechmark sequentially:

1. cd PhTMs/phtms-experiments
2. ./scripts/execute -b seq -s 'genome labyrinth ssca2 kmeans intruder vacation' -t 1 -n 10

- To compile the graph applications with sequential from the phtm_transitioning_experiments/ folder:
You must set the following flags:
-s refers to the applications that will be compiled with.
-q sequential.

1. cd GoTM/gotm-tm-experiments/
2. ./compile -s  'sssp_TM mst_TM bfs_TM bc_TM wcc_TM pagerank_TM' -q 1


- To execute the graph applications sequentially:
you must set the following flags: 
-s refers to the applications that will be executed.
-i the workloads (these should be plased in the GoTM/gotm-tm-experiments/inputs folder, and must be in the galois input format with .gr extension.
-r the number of executions.
-q to execute sequentially

1. cd GoTM/gotm-tm-experiments/
2. ./execute  -s 'sssp_TM mst_TM bfs_TM bc_TM wcc_TM pagerank_TM' -i ' rmat-n30m-m120m soc-LiveJournal1' -r 10 -q 1

Results can be found in the results/ folder.

- To plot results for both benchmarks:

1. cd Plots/
2. python3 results_speedup_graphs.py
3. python3 results_speedup_stamp.py
